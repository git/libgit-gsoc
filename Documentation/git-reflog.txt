git-reflog(1)
=============

NAME
----
git-reflog - Manage reflog information


SYNOPSIS
--------
'git reflog' <subcommand> <options>

DESCRIPTION
-----------
The command takes various subcommands, and different options
depending on the subcommand:

[verse]
git reflog expire [--dry-run] [--stale-fix]
	[--expire=<time>] [--expire-unreachable=<time>] [--all] <refs>...

git reflog [show] [log-options]

Reflog is a mechanism to record when the tip of branches are
updated.  This command is to manage the information recorded in it.

The subcommand "expire" is used to prune older reflog entries.
Entries older than `expire` time, or entries older than
`expire-unreachable` time and are not reachable from the current
tip, are removed from the reflog.  This is typically not used
directly by the end users -- instead, see gitlink:git-gc[1].

The subcommand "show" (which is also the default, in the absense of any
subcommands) will take all the normal log options, and show the log of
the current branch. It is basically an alias for 'git log -g --abbrev-commit
--pretty=oneline', see gitlink:git-log[1].


OPTIONS
-------

--stale-fix::
	This revamps the logic -- the definition of "broken commit"
	becomes: a commit that is not reachable from any of the refs and
	there is a missing object among the commit, tree, or blob
	objects reachable from it that is not reachable from any of the
	refs.
+
This computation involves traversing all the reachable objects, i.e. it
has the same cost as 'git prune'.  Fortunately, once this is run, we
should not have to ever worry about missing objects, because the current
prune and pack-objects know about reflogs and protect objects referred by
them.

--expire=<time>::
	Entries older than this time are pruned.  Without the
	option it is taken from configuration `gc.reflogExpire`,
	which in turn defaults to 90 days.

--expire-unreachable=<time>::
	Entries older than this time and are not reachable from
	the current tip of the branch are pruned.  Without the
	option it is taken from configuration
	`gc.reflogExpireUnreachable`, which in turn defaults to
	30 days.

--all::
	Instead of listing <refs> explicitly, prune all refs.

Author
------
Written by Junio C Hamano <junkio@cox.net>

Documentation
--------------
Documentation by Junio C Hamano and the git-list <git@vger.kernel.org>.

GIT
---
Part of the gitlink:git[7] suite
