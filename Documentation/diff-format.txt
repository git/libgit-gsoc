The output format from "git-diff-index", "git-diff-tree" and
"git-diff-files" are very similar.

These commands all compare two sets of things; what is
compared differs:

git-diff-index <tree-ish>::
        compares the <tree-ish> and the files on the filesystem.

git-diff-index --cached <tree-ish>::
        compares the <tree-ish> and the index.

git-diff-tree [-r] <tree-ish-1> <tree-ish-2> [<pattern>...]::
        compares the trees named by the two arguments.

git-diff-files [<pattern>...]::
        compares the index and the files on the filesystem.


An output line is formatted this way:

------------------------------------------------
in-place edit  :100644 100644 bcd1234... 0123456... M file0
copy-edit      :100644 100644 abcd123... 1234567... C68 file1 file2
rename-edit    :100644 100644 abcd123... 1234567... R86 file1 file3
create         :000000 100644 0000000... 1234567... A file4
delete         :100644 000000 1234567... 0000000... D file5
unmerged       :000000 000000 0000000... 0000000... U file6
------------------------------------------------

That is, from the left to the right:

. a colon.
. mode for "src"; 000000 if creation or unmerged.
. a space.
. mode for "dst"; 000000 if deletion or unmerged.
. a space.
. sha1 for "src"; 0\{40\} if creation or unmerged.
. a space.
. sha1 for "dst"; 0\{40\} if creation, unmerged or "look at work tree".
. a space.
. status, followed by optional "score" number.
. a tab or a NUL when '-z' option is used.
. path for "src"
. a tab or a NUL when '-z' option is used; only exists for C or R.
. path for "dst"; only exists for C or R.
. an LF or a NUL when '-z' option is used, to terminate the record.

<sha1> is shown as all 0's if a file is new on the filesystem
and it is out of sync with the index.

Example:

------------------------------------------------
:100644 100644 5be4a4...... 000000...... M file.c
------------------------------------------------

When `-z` option is not used, TAB, LF, and backslash characters
in pathnames are represented as `\t`, `\n`, and `\\`,
respectively.

diff format for merges
----------------------

"git-diff-tree" and "git-diff-files" can take '-c' or '--cc' option
to generate diff output also for merge commits.  The output differs
from the format described above in the following way:

. there is a colon for each parent
. there are more "src" modes and "src" sha1
. status is concatenated status characters for each parent
. no optional "score" number
. single path, only for "dst"

Example:

------------------------------------------------
::100644 100644 100644 fabadb8... cc95eb0... 4866510... MM	describe.c
------------------------------------------------

Note that 'combined diff' lists only files which were modified from
all parents.


Generating patches with -p
--------------------------

When "git-diff-index", "git-diff-tree", or "git-diff-files" are run
with a '-p' option, they do not produce the output described above;
instead they produce a patch file.  You can customize the creation
of such patches via the GIT_EXTERNAL_DIFF and the GIT_DIFF_OPTS
environment variables.

What the -p option produces is slightly different from the traditional
diff format.

1.   It is preceded with a "git diff" header, that looks like
     this:

       diff --git a/file1 b/file2
+
The `a/` and `b/` filenames are the same unless rename/copy is
involved.  Especially, even for a creation or a deletion,
`/dev/null` is _not_ used in place of `a/` or `b/` filenames.
+
When rename/copy is involved, `file1` and `file2` show the
name of the source file of the rename/copy and the name of
the file that rename/copy produces, respectively.

2.   It is followed by one or more extended header lines:

       old mode <mode>
       new mode <mode>
       deleted file mode <mode>
       new file mode <mode>
       copy from <path>
       copy to <path>
       rename from <path>
       rename to <path>
       similarity index <number>
       dissimilarity index <number>
       index <hash>..<hash> <mode>

3.  TAB, LF, double quote and backslash characters in pathnames
    are represented as `\t`, `\n`, `\"` and `\\`, respectively.
    If there is need for such substitution then the whole
    pathname is put in double quotes.

The similarity index is the percentage of unchanged lines, and
the dissimilarity index is the percentage of changed lines.  It
is a rounded down integer, followed by a percent sign.  The
similarity index value of 100% is thus reserved for two equal
files, while 100% dissimilarity means that no line from the old
file made it into the new one.


combined diff format
--------------------

git-diff-tree and git-diff-files can take '-c' or '--cc' option
to produce 'combined diff', which looks like this:

------------
diff --combined describe.c
index fabadb8,cc95eb0..4866510
--- a/describe.c
+++ b/describe.c
@@@ -98,20 -98,12 +98,20 @@@
	return (a_date > b_date) ? -1 : (a_date == b_date) ? 0 : 1;
  }

- static void describe(char *arg)
 -static void describe(struct commit *cmit, int last_one)
++static void describe(char *arg, int last_one)
  {
 +	unsigned char sha1[20];
 +	struct commit *cmit;
	struct commit_list *list;
	static int initialized = 0;
	struct commit_name *n;

 +	if (get_sha1(arg, sha1) < 0)
 +		usage(describe_usage);
 +	cmit = lookup_commit_reference(sha1);
 +	if (!cmit)
 +		usage(describe_usage);
 +
	if (!initialized) {
		initialized = 1;
		for_each_ref(get_name);
------------

1.   It is preceded with a "git diff" header, that looks like
     this (when '-c' option is used):

       diff --combined file
+
or like this (when '--cc' option is used):

       diff --c file

2.   It is followed by one or more extended header lines
     (this example shows a merge with two parents):

       index <hash>,<hash>..<hash>
       mode <mode>,<mode>..<mode>
       new file mode <mode>
       deleted file mode <mode>,<mode>
+
The `mode <mode>,<mode>..<mode>` line appears only if at least one of
the <mode> is different from the rest. Extended headers with
information about detected contents movement (renames and
copying detection) are designed to work with diff of two
<tree-ish> and are not used by combined diff format.

3.   It is followed by two-line from-file/to-file header

       --- a/file
       +++ b/file
+
Similar to two-line header for traditional 'unified' diff
format, `/dev/null` is used to signal created or deleted
files.

4.   Chunk header format is modified to prevent people from
     accidentally feeding it to `patch -p1`. Combined diff format
     was created for review of merge commit changes, and was not
     meant for apply. The change is similar to the change in the
     extended 'index' header:

       @@@ <from-file-range> <from-file-range> <to-file-range> @@@
+
There are (number of parents + 1) `@` characters in the chunk
header for combined diff format.

Unlike the traditional 'unified' diff format, which shows two
files A and B with a single column that has `-` (minus --
appears in A but removed in B), `+` (plus -- missing in A but
added to B), or `" "` (space -- unchanged) prefix, this format
compares two or more files file1, file2,... with one file X, and
shows how X differs from each of fileN.  One column for each of
fileN is prepended to the output line to note how X's line is
different from it.

A `-` character in the column N means that the line appears in
fileN but it does not appear in the result.  A `+` character
in the column N means that the line appears in the last file,
and fileN does not have that line (in other words, the line was
added, from the point of view of that parent).

In the above example output, the function signature was changed
from both files (hence two `-` removals from both file1 and
file2, plus `++` to mean one line that was added does not appear
in either file1 nor file2).  Also two other lines are the same
from file1 but do not appear in file2 (hence prefixed with ` +`).

When shown by `git diff-tree -c`, it compares the parents of a
merge commit with the merge result (i.e. file1..fileN are the
parents).  When shown by `git diff-files -c`, it compares the
two unresolved merge parents with the working tree file
(i.e. file1 is stage 2 aka "our version", file2 is stage 3 aka
"their version").
