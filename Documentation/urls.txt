GIT URLS[[URLS]]
----------------

One of the following notations can be used
to name the remote repository:

===============================================================
- rsync://host.xz/path/to/repo.git/
- http://host.xz/path/to/repo.git/
- https://host.xz/path/to/repo.git/
- git://host.xz/path/to/repo.git/
- git://host.xz/~user/path/to/repo.git/
- ssh://{startsb}user@{endsb}host.xz/path/to/repo.git/
- ssh://{startsb}user@{endsb}host.xz/~user/path/to/repo.git/
- ssh://{startsb}user@{endsb}host.xz/~/path/to/repo.git
===============================================================

SSH is the default transport protocol over the network.  You can
optionally specify which user to log-in as, and an alternate,
scp-like syntax is also supported.  Both syntaxes support
username expansion, as does the native git protocol. The following
three are identical to the last three above, respectively:

===============================================================
- {startsb}user@{endsb}host.xz:/path/to/repo.git/
- {startsb}user@{endsb}host.xz:~user/path/to/repo.git/
- {startsb}user@{endsb}host.xz:path/to/repo.git
===============================================================

To sync with a local directory, you can use:

===============================================================
- /path/to/repo.git/
- file:///path/to/repo.git/
===============================================================

They are mostly equivalent, except when cloning.  See
gitlink:git-clone[1] for details.
