git-rm(1)
=========

NAME
----
git-rm - Remove files from the working tree and from the index

SYNOPSIS
--------
'git-rm' [-f] [-n] [-r] [--cached] [--ignore-unmatch] [--quiet] [--] <file>...

DESCRIPTION
-----------
Remove files from the working tree and from the index.  The
files have to be identical to the tip of the branch, and no
updates to its contents must have been placed in the staging
area (aka index).  When --cached is given, the staged content has to
match either the tip of the branch *or* the file on disk.


OPTIONS
-------
<file>...::
	Files to remove.  Fileglobs (e.g. `*.c`) can be given to
	remove all matching files.  Also a leading directory name
	(e.g. `dir` to add `dir/file1` and `dir/file2`) can be
	given to remove all files in the directory, recursively,
	but this requires `-r` option to be given for safety.

-f::
	Override the up-to-date check.

-n::
        Don't actually remove the file(s), just show if they exist in
        the index.

-r::
        Allow recursive removal when a leading directory name is
        given.

\--::
	This option can be used to separate command-line options from
	the list of files, (useful when filenames might be mistaken
	for command-line options).

\--cached::
	This option can be used to tell the command to remove
	the paths only from the index, leaving working tree
	files.

\--ignore-unmatch::
	Exit with a zero status even if no files matched.

\--quiet::
	git-rm normally outputs one line (in the form of an "rm" command)
	for each file removed. This option suppresses that output.


DISCUSSION
----------

The list of <file> given to the command can be exact pathnames,
file glob patterns, or leading directory name.  The command
removes only the paths that is known to git.  Giving the name of
a file that you have not told git about does not remove that file.


EXAMPLES
--------
git-rm Documentation/\\*.txt::
	Removes all `\*.txt` files from the index that are under the
	`Documentation` directory and any of its subdirectories.
+
Note that the asterisk `\*` is quoted from the shell in this
example; this lets the command include the files from
subdirectories of `Documentation/` directory.

git-rm -f git-*.sh::
	Remove all git-*.sh scripts that are in the index.
	Because this example lets the shell expand the asterisk
	(i.e. you are listing the files explicitly), it
	does not remove `subdir/git-foo.sh`.

See Also
--------
gitlink:git-add[1]

Author
------
Written by Linus Torvalds <torvalds@osdl.org>

Documentation
--------------
Documentation by Junio C Hamano and the git-list <git@vger.kernel.org>.

GIT
---
Part of the gitlink:git[7] suite
