#ifndef LT_COMMIT_H
#define LT_COMMIT_H

struct git_commit;

int git_commit_message(struct git_commit *commit,
		       const char **ret, size_t *len);
int git_commit_committer_email(struct git_commit *commit,
			       const char **ret, size_t *len);
int git_commit_committer_name(struct git_commit *commit,
			      const char **ret, size_t *len);
int git_commit_committer_date(struct git_commit *commit,
			      time_t *com_time, int *com_tz);
int git_commit_author_name(struct git_commit *commit,
			   const char **ret, size_t *len);
int git_commit_author_email(struct git_commit *commit,
			    const char **ret, size_t *len);
int git_commit_author_date(struct git_commit *commit,
			   time_t *com_time, int *com_tz);
int git_commit_id(struct git_commit *commit, unsigned char *sha1);
int git_commit_tree(struct git_commit *commit, unsigned char *sha1);
const char *git_commit_raw(struct git_commit *commit);
void __git_commit_free(struct git_commit *commit);
void git_commit_free(struct git_commit *commit);
void __git_commit_init(struct git_commit *commit);
struct git_commit *git_commit_init(void);
struct git_commit *git_commit_lookup(const unsigned char *sha1);
int git_commit_equal(const struct git_commit *a,const struct git_commit *b);
struct commit *__git_commit_obj(struct git_commit *commit);
void __git_commit_obj_set(struct git_commit *commit, struct commit *new_com);
char *git_commit_to_str(const char *src, size_t len);

#endif /* LT_COMMIT_H */
