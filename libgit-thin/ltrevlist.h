#ifndef LT_REVLIST_H
#define LT_REVLIST_H

struct git_revlist_opt;

int git_revlist_next(struct git_revlist_opt *opt, struct git_commit *commit);
void git_revlist_free(struct git_revlist_opt *opt);
struct git_revlist_opt *git_revlist_init(void);
int git_revlist_include(struct git_revlist_opt *opt, unsigned char *sha1);
int git_revlist_exclude(struct git_revlist_opt *opt, unsigned char *sha1);
int git_revlist_reverse(struct git_revlist_opt *opt);
int git_revlist_show_merges(struct git_revlist_opt *opt);
int git_revlist_max_count(struct git_revlist_opt *opt, size_t count);

#endif /* LT_REVLIST_H */
