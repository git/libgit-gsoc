from distutils.core import setup, Extension

libgit_thin = '..'
libgit = libgit_thin + '/..'
xdiff = libgit + '/xdiff'

pygit_module = Extension('pygit',
		include_dirs = [libgit_thin, libgit],
		libraries = ['git-thin', 'git', 'z', 'crypto', 'xdiff'],
		library_dirs = [libgit_thin, libgit, xdiff],
		sources = ['pygit.c'])

setup (name = 'PyGIT',
		ext_modules = [pygit_module])
