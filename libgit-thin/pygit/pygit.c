/*
 * PyGit: A GIT binding for python
 * 
 * [ Dedicated to the people who died in the worst Brazil's airplane
 *   accident, at São Paulo's Congonhas airport on 2007/07/17 ]
 * 
 * This software is licensed under the GPLv2.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <Python.h>
#include "structmember.h"

#include <libgit-thin.h>

static PyObject *PyGitError;

PyMODINIT_FUNC initpygit(void);

/*
 * Useful helpers for all the types
 */

#define UNUSED(x) (x = x)

static PyObject *
pygit_error(void)
{
	return PyErr_Format(PyGitError, "[Errno: %d] %s", errno,
			    strerror(errno));
}

static int
pyarg_to_sha1(PyObject *args, unsigned char *sha1)
{
	const char *hex;

	if (!PyArg_ParseTuple(args, "s", &hex))
		return -1;

	if (git_hex_to_sha1(hex, sha1)) {
		pygit_error();
		return -1;
	}

	return 0;
}

static PyObject *
sha1_to_pystr(unsigned char *sha1)
{
	char hex[GIT_HEX_LENGTH + 1];

	if (git_sha1_to_hex(sha1, hex))
		return pygit_error();

	return PyString_FromString(hex);
}

/*
 * Commit Type
 */

typedef struct {
	PyObject_HEAD
	struct git_commit *commit;
} GitCommitObject;

static PyObject *
pygit_call_commit_op(struct git_commit *commit,
		     int (*commit_op)(struct git_commit *,
				      const char **, size_t *))
{
	int err;
	size_t len;
	const char *p;

	err = commit_op(commit, &p, &len);
	if (err)
		return pygit_error();

	return PyString_FromStringAndSize(p, len);
}

PyDoc_STRVAR(cmessage_doc,
"commit.message() -> string\n\
\n\
Return the commit's message.");

static PyObject *
commit_message(GitCommitObject *self, PyObject *args)
{
	UNUSED(args);
	return pygit_call_commit_op(self->commit, git_commit_message);
}

PyDoc_STRVAR(ccommitter_email_doc,
"commit.committer_email() -> string\n\
\n\
Return the commit's committer e-mail.");

static PyObject *
commit_committer_email(GitCommitObject *self, PyObject *args)
{
	UNUSED(args);
	return pygit_call_commit_op(self->commit, git_commit_committer_email);
}

PyDoc_STRVAR(ccommitter_name_doc,
"commit.committer_name() -> string\n\
\n\
Return the commit's committer name.");

static PyObject *
commit_committer_name(GitCommitObject *self, PyObject *args)
{
	UNUSED(args);
	return pygit_call_commit_op(self->commit, git_commit_committer_name);
}

PyDoc_STRVAR(cauthor_email_doc,
"commit.author_email() -> string\n\
\n\
Return the commit's author e-mail.");

static PyObject *
commit_author_email(GitCommitObject *self, PyObject *args)
{
	UNUSED(args);
	return pygit_call_commit_op(self->commit, git_commit_author_email);
}

PyDoc_STRVAR(cauthor_name_doc,
"commit.author_name() -> string\n\
\n\
Return the commit's author name.");

static PyObject *
commit_author_name(GitCommitObject *self, PyObject *args)
{
	UNUSED(args);
	return pygit_call_commit_op(self->commit, git_commit_author_name);
}

PyDoc_STRVAR(commit_raw_doc,
"commit.buffer() -> string\n\
\n\
Return the commit's raw buffer.");

static PyObject *
commit_raw(GitCommitObject *self, PyObject *args)
{
	const char *ret;

	UNUSED(args);

	ret = git_commit_raw(self->commit);
	if (!ret)
		return pygit_error();

	return PyString_FromString(ret);
}

static PyObject *
pygit_call_commit_op_sha1(struct git_commit *commit,
			  int (*commit_op)(struct git_commit *,
					   unsigned char *sha1))
{
	int err;
	unsigned char sha1[GIT_SHA1_SIZE];

	err = commit_op(commit, sha1);
	if (err)
		return pygit_error();

	return sha1_to_pystr(sha1);
}

PyDoc_STRVAR(commit_id_doc,
"commit.id() -> string\n\
\n\
Return the commit's SHA1 hex representation.");

static PyObject *
commit_id(GitCommitObject *self, PyObject *args)
{
	UNUSED(args);
	return pygit_call_commit_op_sha1(self->commit, git_commit_id);
}

PyDoc_STRVAR(commit_tree_doc,
"commit.tree() -> string\n\
\n\
Return the commit's tree SHA1 hex representation.");

static PyObject *
commit_tree(GitCommitObject *self, PyObject *args)
{
	UNUSED(args);
	return pygit_call_commit_op_sha1(self->commit, git_commit_tree);
}

static PyMethodDef commit_methods[] = {
	{"message", (PyCFunction) commit_message, METH_VARARGS, cmessage_doc},
	{"id", (PyCFunction) commit_id, METH_VARARGS, commit_id_doc},
	{"tree", (PyCFunction) commit_tree, METH_VARARGS, commit_tree_doc},
	{"buffer", (PyCFunction) commit_raw, METH_VARARGS, commit_raw_doc},
	{"committer_email", (PyCFunction) commit_committer_email,
			METH_VARARGS, ccommitter_email_doc},
	{"committer_name", (PyCFunction) commit_committer_name, METH_VARARGS,
			ccommitter_name_doc},
	{"author_email", (PyCFunction) commit_author_email, METH_VARARGS,
	                cauthor_email_doc},
	{"author_name", (PyCFunction) commit_author_name, METH_VARARGS,
	                cauthor_name_doc},
	{NULL, NULL, 0, NULL}
};

static void
commit_dealloc(GitCommitObject *self)
{
	git_commit_free(self->commit);
	self->ob_type->tp_free((PyObject*) self);
}

static int
commit_compare(GitCommitObject *a, GitCommitObject *b)
{
	int ret;
	time_t a_time, b_time;

	if (git_commit_equal(a->commit, b->commit))
		return 0;

	ret = git_commit_committer_date(a->commit, &a_time, NULL);
	if (ret) {
		pygit_error();
		return -1;
	}

	ret = git_commit_committer_date(b->commit, &b_time, NULL);
	if (ret) {
		pygit_error();
		return -1;
	}

	ret = a_time < b_time ? -1 : 1;
	return ret;
}

static PyTypeObject Git_Commit_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                                  /* ob_size */
    "pygit.GitCommit",                  /* tp_name */
    sizeof(GitCommitObject),            /* tp_basicsize */
    0,                                  /* tp_itemsize */
    (destructor)commit_dealloc,         /* tp_dealloc */
    0,                                  /* tp_print */
    0,                                  /* tp_getattr */
    0,					/* tp_setattr */
    (cmpfunc)commit_compare,	        /* tp_compare */
    0,					/* tp_repr */
    0,					/* tp_as_number */
    0,					/* tp_as_sequence */
    0,					/* tp_as_mapping */
    0,					/* tp_hash */
    0,					/* tp_call */
    0,					/* tp_str */
    0,					/* tp_getattro */
    0,					/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,		        /* tp_flags */
    0,	                 		/* tp_doc */
    0,					/* tp_traverse */
    0,					/* tp_clear */
    0,					/* tp_richcompare */
    0,					/* tp_weaklistoffset */
    0,					/* tp_iter */
    0,					/* tp_iternext */
    commit_methods,	       		/* tp_methods */
    0,           			/* tp_members */
    0,					/* tp_getset */
    0,					/* tp_base */
    0,					/* tp_dict */
    0,					/* tp_descr_get */
    0,					/* tp_descr_set */
    0,					/* tp_dictoffset */
    0,					/* tp_init */
    0,					/* tp_alloc */
    0,					/* tp_new */
};

/*
 * Revision list type
 */

typedef struct {
	PyObject_HEAD
	PyObject *count;
	struct git_revlist_opt *opt;
} GitRevListObject;

static PyObject *
revlist_add_commit(GitRevListObject *self, PyObject *args, int exclude)
{
	int err;
	unsigned char sha1[GIT_SHA1_SIZE];

	err = pyarg_to_sha1(args, sha1);
	if (err)
		return pygit_error();

	if (exclude)
		err = git_revlist_exclude(self->opt, sha1);
	else
		err = git_revlist_include(self->opt, sha1);

	if (err)
		return pygit_error();

	Py_RETURN_NONE;
}

PyDoc_STRVAR(rev_include_doc,
"revlist.include(id) -> None\n\
\n\
Include the commit (repesented by id) in the revision listing.");

static PyObject *
revlist_include(GitRevListObject *self, PyObject *args)
{
	return revlist_add_commit(self, args, 0);
}

PyDoc_STRVAR(rev_exclude_doc,
"revlist.exclude(id) -> None\n\
\n\
Exclude the commit (repesented by id) from the revision listing.");

static PyObject *
revlist_exclude(GitRevListObject *self, PyObject *args)
{
	return revlist_add_commit(self, args, 1);
}

PyDoc_STRVAR(rev_reverse_doc,
"revlist.reserve() -> None\n\
\n\
If called, sets the 'reserve' revision listing option.");

static PyObject *
revlist_reverse(GitRevListObject *self, PyObject *args)
{
	int err;

	UNUSED(args);

	err = git_revlist_reverse(self->opt);
	if (err)
		return pygit_error();

	Py_RETURN_NONE;
}

PyDoc_STRVAR(rev_show_merges_doc,
"revlist.show_merges() -> None\n\
\n\
If called, sets the 'show merges' revision listing option.");

static PyObject *
revlist_show_merges(GitRevListObject *self, PyObject *args)
{
	int err;

	UNUSED(args);

	err = git_revlist_show_merges(self->opt);
	if (err)
		return pygit_error();

	Py_RETURN_NONE;
}

static PyMethodDef revlist_methods[] = {
	{"include", (PyCFunction) revlist_include, METH_VARARGS,
			rev_include_doc},
	{"exclude", (PyCFunction) revlist_exclude, METH_VARARGS,
			rev_exclude_doc},
	{"reverse", (PyCFunction) revlist_reverse, METH_NOARGS,
			rev_reverse_doc},
	{"show_merges", (PyCFunction) revlist_show_merges, METH_NOARGS,
			rev_show_merges_doc},
	{NULL, NULL, 0, NULL}
};

static PyObject *
revlist_get_count(GitRevListObject *self, void *closure)
{
	UNUSED(closure);

	Py_INCREF(self->count);
	return self->count;
}

static int
revlist_set_count(GitRevListObject *self, PyObject *value, void *closure)
{
	int err;
	long count;

	UNUSED(closure);

	if (!value) {
		PyErr_SetString(PyGitError,
				"Cannot delete the count attribute");
		return -1;
	}

	if (!PyInt_Check(value)) {
		PyErr_SetString(PyGitError,
				"The count attribute must be an int");
		return -1;
	}

	count = PyInt_AS_LONG(value);
	err = git_revlist_max_count(self->opt, (size_t) count);
	if (err) {
		pygit_error();
		return -1;
	}

	Py_DECREF(self->count);
	Py_INCREF(value);
	self->count = value;

	return 0;
}

static PyGetSetDef revlist_getseters[] = {
	{"count",
	(getter)revlist_get_count, (setter)revlist_set_count,
	"number of commits to walk through", NULL},
	{NULL, NULL, NULL, NULL, NULL},
};

static void
revlist_dealloc(GitRevListObject *self)
{
	Py_DECREF(self->count);
	git_revlist_free(self->opt);
	self->ob_type->tp_free((PyObject*) self);
}

static PyObject *
revlist_iter(GitRevListObject *self)
{
	Py_INCREF(self);
	return (PyObject *) self;
}

static PyObject *
revlist_iternext(GitRevListObject *self)
{
	int ret;
	GitCommitObject *m;
	struct git_commit *commit;

	m = PyObject_New(GitCommitObject, &Git_Commit_Type);
	if (!m)
		return NULL;

	ret = PyType_Ready(&Git_Commit_Type);
	if (ret) {
		PyObject_DEL(m);
		return NULL;
	}

	commit = git_commit_init();
	if (!commit) {
		PyObject_DEL(m);
		return pygit_error();
	}

	ret = git_revlist_next(self->opt, commit);
	if (ret != 1) {
		git_commit_free(commit);
		PyObject_DEL(m);
		return NULL;
	}

	m->commit = commit;
	return (PyObject *) m;
}

static PyTypeObject Git_RevList_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                                  /* ob_size */
    "pygit.GitRevList",                 /* tp_name */
    sizeof(GitRevListObject),           /* tp_basicsize */
    0,                                  /* tp_itemsize */
    (destructor)revlist_dealloc,        /* tp_dealloc */
    0,                                  /* tp_print */
    0,                                  /* tp_getattr */
    0,					/* tp_setattr */
    0,					/* tp_compare */
    0,					/* tp_repr */
    0,					/* tp_as_number */
    0,					/* tp_as_sequence */
    0,					/* tp_as_mapping */
    0,					/* tp_hash */
    0,					/* tp_call */
    0,					/* tp_str */
    0,					/* tp_getattro */
    0,					/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,		        /* tp_flags */
    0,	                 		/* tp_doc */
    0,					/* tp_traverse */
    0,					/* tp_clear */
    0,					/* tp_richcompare */
    0,					/* tp_weaklistoffset */
    (getiterfunc)revlist_iter,	        /* tp_iter */
    (iternextfunc)revlist_iternext,	/* tp_iternext */
    revlist_methods,      		/* tp_methods */
    0,	                		/* tp_members */
    revlist_getseters,			/* tp_getset */
    0,					/* tp_base */
    0,					/* tp_dict */
    0,					/* tp_descr_get */
    0,					/* tp_descr_set */
    0,					/* tp_dictoffset */
    0,					/* tp_init */
    0,					/* tp_alloc */
    0,					/* tp_new */
};

/*
 * Repository type
 */

typedef struct {
	PyObject_HEAD
	PyObject *path;
} GitRepoObject;

static PyObject *
repo_read_obj(PyObject *args,
	      int (*read_obj)(unsigned char *sha1, void **buf, size_t *len))
{
	int err;
	void *buf;
	PyObject *ret;
	unsigned char sha1[GIT_SHA1_SIZE];

	err = pyarg_to_sha1(args, sha1);
	if (err)
		return NULL;

	err = read_obj(sha1, &buf, NULL);
	if (err)
		return pygit_error();

	ret = PyString_FromString((char *) buf);
	free(buf);

	return ret;
}

static PyObject *
repo_head_commit(GitRepoObject *self, PyObject *args)
{
	int err;
	unsigned char sha1[GIT_SHA1_SIZE];

	UNUSED(self);
	UNUSED(args);

	err = git_repo_head(sha1);
	if (err)
		return pygit_error();

	return sha1_to_pystr(sha1);
}

static PyObject *
repo_read_commit(GitRepoObject *self, PyObject *args)
{
	UNUSED(self);
	return repo_read_obj(args, git_repo_commit_read);
}

static PyObject *
repo_read_blob(GitRepoObject *self, PyObject *args)
{
	UNUSED(self);
	return repo_read_obj(args, git_repo_blob_read);
}

static PyObject *
repo_lookup_commit(GitRepoObject *self, PyObject *args)
{
	int err;
	GitCommitObject *m;
	struct git_commit *commit;
	unsigned char sha1[GIT_SHA1_SIZE];

	UNUSED(self);

	err = pyarg_to_sha1(args, sha1);
	if (err)
		return NULL;

	m = PyObject_New(GitCommitObject, &Git_Commit_Type);
	if (!m) 
		return NULL;

	err = PyType_Ready(&Git_Commit_Type);
	if (err) {
		PyObject_DEL(m);
		return NULL;
	}

	commit = git_commit_lookup(sha1);
	if (!commit) {
		PyObject_DEL(m);
		return pygit_error();
	}
	m->commit = commit;

	return (PyObject *) m;
}

static PyObject *
repo_translate_ref(GitRepoObject *self, PyObject *args)
{
	int err;
	const char *ref;
	unsigned char sha1[GIT_SHA1_SIZE];

	UNUSED(self);

	if (!PyArg_ParseTuple(args, "s", &ref))
		return NULL;

	err = git_repo_translate_ref(ref, sha1);
	if (err)
		return pygit_error();

	return sha1_to_pystr(sha1);
}

static PyObject *
repo_revlist(GitRepoObject *self, PyObject *args)
{
	int err;
	GitRevListObject *m;

	UNUSED(self);
	UNUSED(args);

	m = PyObject_New(GitRevListObject, &Git_RevList_Type);
	if (!m)
		return NULL;

	err = PyType_Ready(&Git_RevList_Type);
	if (err) {
		PyObject_DEL(m);
		return NULL;
	}

	m->opt = git_revlist_init();
	if (!m->opt) {
		PyObject_DEL(m);
		return pygit_error();
	}

	m->count = PyInt_FromLong(0);
	if (!m->count) {
		git_revlist_free(m->opt);
		PyObject_DEL(m);
		return NULL;
	}

	return (PyObject *) m;
}

static PyMethodDef repo_methods[] = {
	{"read_commit", (PyCFunction) repo_read_commit, METH_VARARGS, NULL},
	{"read_blob", (PyCFunction) repo_read_blob, METH_VARARGS, NULL},
	{"lookup_commit", (PyCFunction) repo_lookup_commit, METH_VARARGS,NULL},
	{"head_commit", (PyCFunction) repo_head_commit, METH_NOARGS, NULL},
	{"translate_ref", (PyCFunction) repo_translate_ref,METH_VARARGS, NULL},
	{"revlist", (PyCFunction) repo_revlist, METH_VARARGS, NULL},
	{NULL, NULL, 0, NULL}
};

static PyMemberDef repo_members[] = {
	{"path", T_OBJECT_EX, offsetof(GitRepoObject, path), 0,
	 "repository's path"},
	{NULL, 0, 0, 0, NULL}
};

static void
repo_dealloc(GitRepoObject *self)
{
	Py_DECREF(self->path);
	self->ob_type->tp_free((PyObject*) self);
}

static PyTypeObject Git_Repo_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                                  /* ob_size */
    "pygit.GitRepo",                    /* tp_name */
    sizeof(GitRepoObject),              /* tp_basicsize */
    0,                                  /* tp_itemsize */
    (destructor)repo_dealloc,           /* tp_dealloc */
    0,                                  /* tp_print */
    0,                                  /* tp_getattr */
    0,					/* tp_setattr */
    0,					/* tp_compare */
    0,					/* tp_repr */
    0,					/* tp_as_number */
    0,					/* tp_as_sequence */
    0,					/* tp_as_mapping */
    0,					/* tp_hash */
    0,					/* tp_call */
    0,					/* tp_str */
    0,					/* tp_getattro */
    0,					/* tp_setattro */
    0,					/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,		        /* tp_flags */
    0,	                 		/* tp_doc */
    0,					/* tp_traverse */
    0,					/* tp_clear */
    0,					/* tp_richcompare */
    0,					/* tp_weaklistoffset */
    0,                   		/* tp_iter */
    0,                           	/* tp_iternext */
    repo_methods,	       		/* tp_methods */
    repo_members,			/* tp_members */
    0,					/* tp_getset */
    0,					/* tp_base */
    0,					/* tp_dict */
    0,					/* tp_descr_get */
    0,					/* tp_descr_set */
    0,					/* tp_dictoffset */
    0,					/* tp_init */
    0,					/* tp_alloc */
    0,					/* tp_new */
};

/*
 * Module's stuff
 */

static PyObject *
pygit_open(PyObject *self, PyObject *args)
{
	int err;
	const char *dir;
	GitRepoObject *m;

	UNUSED(self);

	if (!PyArg_ParseTuple(args, "s", &dir))
		return NULL;

	err = git_repo_open(dir);
	if (err)
		return pygit_error();

	m = PyObject_New(GitRepoObject, &Git_Repo_Type);
	if (!m)
		return NULL;

	err = PyType_Ready(&Git_Repo_Type);
	if (err) {
		PyObject_DEL(m);
		return NULL;
	}

	m->path = PyString_FromString(dir);
	if (!m->path) {
		PyObject_DEL(m);
		return NULL;
	}

	return (PyObject *) m;
}

static PyMethodDef pygit_methods[] = {
	{ "open", pygit_open, METH_VARARGS, "Open a GIT repository" },
	{ NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
initpygit(void)
{
	PyObject *m;

	m = Py_InitModule("pygit", pygit_methods);
	if (!m)
		return;

	PyGitError = PyErr_NewException("pygit.error", NULL, NULL);
	if (!PyGitError)
		return ;

	Py_INCREF(PyGitError);
	PyModule_AddObject(m, "error", PyGitError);
}
