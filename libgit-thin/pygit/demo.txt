1. Retrieve a commit (raw) buffer
---------------------------------

>>> import pygit
>>> repo = pygit.open('/tmp/simple-repo')
>>> print repo.read_commit('86eb4867dc6983d11e8c8a691a3c62346d87e451')
tree 79321ed405490e585cd9a2b79f18a915d6d68a96
author Gjorg Berisha <gberisha@brokenapril.org> 1165971600 -0200
committer Charles Bukowski <buko@massa.com.br> 1165971600 -0200

Introduces hello.txt

>>> 

2. Retrieve a blob
------------------

>>> import pygit
>>> repo = pygit.open('/tmp/simple-repo')
>>> print repo.read_blob('b45ef6fec89518d314f546fd6c3025367b721684')
Hello, World!
>>>

3. Some commit methods
----------------------

>>> import pygit
>>> repo = pygit.open('/home/lcapitulino/src/kernels/upstream/linux-2.6')
>>> com = repo.lookup_commit('4fbef206daead133085fe33905f5e842d38fb8da')
>>> print com.id()
4fbef206daead133085fe33905f5e842d38fb8da
>>> print com.message()
nfsd: fix nfsd_vfs_read() splice actor setup

When nfsd was transitioned to use splice instead of sendfile() for data
transfers, a line setting the page index was lost. Restore it, so that
nfsd is functional when that path is used.

Signed-off-by: Jens Axboe <jens.axboe@oracle.com>
Signed-off-by: Linus Torvalds <torvalds@linux-foundation.org>

>>>

4. Commit comparison
--------------------

>>> import pygit
>>> repo = pygit.open('/home/lcapitulino/src/kernels/upstream/linux-2.6')
>>> com1 = repo.lookup_commit(repo.head_commit())
>>> com2 = repo.lookup_commit('4fbef206daead133085fe33905f5e842d38fb8da')
>>> if com1 > com2:
...  print 'com1 is more recent'
... 
com1 is more recent
>>> 

5. Show five commits starting at HEAD, show merges as well
----------------------------------------------------------

>>> import pygit
>>> repo = pygit.open('/home/lcapitulino/src/kernels/upstream/linux-2.6')
>>> revl = repo.revlist()
>>> revl.include(repo.head_commit())
>>> revl.show_merges()
>>> revl.count = 5
>>> for commit in revl:
...  print commit.id()
... 
8d9107e8c50e1c4ff43c91c8841805833f3ecfb9
16cefa8c3863721fd40445a1b34dea18cd16ccfe
4fbef206daead133085fe33905f5e842d38fb8da
4fd885170bf13841ada921495b7b00c4b9971cf9
af09f1e4b3214569de93bc9309c35014e5c8a3d0
>>> 
