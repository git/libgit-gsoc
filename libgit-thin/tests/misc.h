#ifndef MISC_H
#define MISC_H

#include <sys/types.h>

int spec_file_read(const char *file);
char *spec_file_get(const char *var, size_t *size);
void spec_file_get_checked(const char *var, char **ret, size_t *len);

#endif /* MISC_H */
