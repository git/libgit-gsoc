/*
 * GIT library-thin tests main file.
 *
 * Note that *all* the tests here depends on a special GIT repositories
 * layout to work.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include "tests.h"

const char *git_repository_dir1;
const char *git_repository_dir2;

static Suite *bogus_test_suite(void)
{
	/*
	 * Only to make check print status nicely
	 */
	return suite_create("");
}

int main(int argc, char *argv[])
{
	SRunner *sr;
	int nf, err;

	if (argc != 3) {
		fprintf(stderr,
			"You must specify two git repos for testing\n");
		exit(1);
	}

	err = spec_file_read("./test-spec.txt");
	if (err) {
		perror("read_spec_file()");
		exit(1);
	}

	git_repository_dir1 = argv[1];
	git_repository_dir2 = argv[2];

	sr = srunner_create(bogus_test_suite());
	srunner_add_suite(sr, ltsha1_test_suite());
	srunner_add_suite(sr, ltrepo_test_suite());
	srunner_add_suite(sr, ltcommit_test_suite());
	srunner_add_suite(sr, ltrevlist_test_suite());

	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
