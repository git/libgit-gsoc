#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <check.h>
#include "misc.h"

static void *spec_file_contents;

int spec_file_read(const char *file)
{
	int err, fd;
	struct stat buf;

	fd = open(file, O_RDONLY);
	if (fd < 0)
		return -1;

	buf.st_size = 0;
	err = fstat(fd, &buf);
	if (err < 0)
		goto out_err;

	spec_file_contents = mmap(0, buf.st_size, PROT_READ, MAP_PRIVATE,
				  fd, 0);
	if (spec_file_contents == MAP_FAILED)
		goto out_err;

	return 0;

out_err:
	close(fd);
	return -1;
}

char *spec_file_get(const char *var, size_t *len)
{
	char *str, *endp, *p;

	if (!var || !len)
		goto out_err;

	p = strstr(spec_file_contents, var);
	if (!p)
		goto out_err;

	p = strstr(p, "=\"");
	if (!p)
		goto out_err;

	p += 2;
	if (!p)
		goto out_err;

	endp = strchr(p, '"');
	if (!endp)
		goto out_err;

	*len = endp - p + 1;
	str = malloc(*len);
	if (!str)
		goto out_err;

	memcpy(str, p, *len - 1);
	str[*len - 1] = '\0';

	return str;

out_err:
	if (len)
		*len = 0;
	errno = EINVAL;
	return NULL;
}

void spec_file_get_checked(const char *var, char **ret, size_t *len)
{
	int bytes;
	size_t mlen;
	char err_msg[128];

	fail_unless(var != NULL, "var is NULL");

	bytes = snprintf(err_msg, sizeof(err_msg),
			 "spec_file_get_checked(%s)", var);
	fail_unless(bytes < (int) sizeof(err_msg),
		    "snprintf() too smal for %s",var);

	fail_unless(ret != NULL, "%s: ret is NULL", err_msg);

	*ret = spec_file_get(var, &mlen);
	fail_unless(*ret != NULL, "%s: %s\n", err_msg, strerror(errno));
	fail_unless(mlen > 0, "%s: len is %d\n", err_msg, mlen);

	if (len)
		*len = mlen;
}
