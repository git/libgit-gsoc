#
# libgit-thin's test data specification
#
# Only change the lines below if you know what you're doing!
#

# Basic test-suite data
TEST_USER1_NAME="Gjorg Berisha"
TEST_USER1_EMAIL="gberisha@brokenapril.org"
TEST_USER1_AUTHOR="Gjorg Berisha <gberisha@brokenapril.org>"
TEST_COMM1_NAME="Charles Bukowski"
TEST_COMM1_EMAIL="buko@massa.com.br"
TEST_FILE1="hello.txt"
TEST_FILE1_CONTENTS="Hello, World!"
TEST_FILE1_BLOB_SHA1="b45ef6fec89518d314f546fd6c3025367b721684"
TEST_FILE1_COMMIT="Introduces hello.txt"
TEST_COMMIT1_ID="86eb4867dc6983d11e8c8a691a3c62346d87e451"
TEST_TREE1_ID="79321ed405490e585cd9a2b79f18a915d6d68a96"
TEST_TAG1_NAME="the-pixies"

# Simple test-suite data
TEST_USER2_NAME="Luiz Gabriel Do Vale Capitulino"
TEST_USER2_EMAIL="gcapitulino@foo.com"
TEST_COMM2_NAME="Luiz Fernando N. Capitulino"
TEST_COMM2_EMAIL="lcapitulino@foo.com"
TEST_COMM2_NAME="Sheyla Vale"
TEST_COMM2_EMAIL="sheyla@foo.com"

TEST_FILE2="casa.txt"
TEST_FILE2_CONTENTS="Era uma casa muito engracada..."
TEST_FILE2_CONTENTS2="Nao tinha teto, nao tinha nada"
TEST_FILE2_COMMIT1="Introduces casa.txt"
TEST_FILE2_COMMIT2="casa.txt: Next line in the poem"
TEST_FILE2_COMMIT_ID="00283cb3b2349aa6c3169f182f4ef686753f1b16"

TEST_FILE3="adoniram.txt"
TEST_FILE3_CONTENTS="
Si o senhor não istá lembrado\n\
Dá licença de contá\n\
Que aqui onde agora está\n\
Esse edifício arto\n\
Era uma casa véia\n\
Um palacete abandonado\n\
Foi aqui seu moço\n\
Que eu, Mato Grosso e o Joca\n\
Construimos nossa maloca"
TEST_FILE3_COMMIT3="Introduces adoniram.txt"
