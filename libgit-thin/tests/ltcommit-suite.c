#include "tests.h"

START_TEST(git_commit_init_test)
{
	struct git_commit *commit;

	commit = git_commit_init();
	fail_unless(commit != NULL, "commit is NULL\n");

	free(commit);
}
END_TEST

START_TEST(git_commit_free_test)
{
	struct git_commit *commit;

	commit = git_commit_init();
	fail_unless(commit != NULL, "commit is NULL\n");

	git_commit_free(commit);
}
END_TEST

START_TEST(git_commit_to_str_test)
{
	char *result;
	const char name[] = "fernando";
	const char orig[] = "fernando\n\ncapitulino";

	result = git_commit_to_str(orig, strlen(name));
	fail_unless(result != NULL, "git_commit_to_str(): %s\n",
		    strerror(errno));
	fail_unless(!memcmp(result, name, strlen(name)), "%s != %s\n",
		    result, name);

	free(result);
}
END_TEST

static struct git_commit *com_commit;

static void com_setup(void)
{
	int ret;
	unsigned char sha1[GIT_SHA1_SIZE];

	ret = git_repo_open(git_repository_dir1);
	fail_unless(ret == 0, "Can't open repository: %s\n", strerror(errno));

	ret = git_repo_head(sha1);
	fail_unless(ret == 0, "git_repo_head(): %s\n", strerror(errno));

	/*
	 * This should be tested before using in 'production' like
	 * that
	 */
	com_commit = git_commit_lookup(sha1);
	fail_unless(com_commit != NULL, "git_commit_lookup(): %s\n",
		    strerror(errno));
}

static void com_teardown(void)
{
	git_commit_free(com_commit);
}

START_TEST(git_commit_raw_test)
{
	size_t len = 0;
	char *author;
	const char *raw_buf;

	spec_file_get_checked("TEST_USER1_AUTHOR", &author, &len);

	/*
	 * XXX: should check the whole buffer
	 */
	raw_buf = git_commit_raw(com_commit);
	fail_unless(raw_buf != NULL, "raw_buf is NULL\n");
	fail_unless(strstr(raw_buf, author) != NULL, "raw_buf: %s", raw_buf);

	free(author);
}
END_TEST

START_TEST(git_commit_id_test)
{
	int ret;
	char *hex;
	unsigned char sha1[GIT_SHA1_SIZE];
	unsigned char com_sha1[GIT_SHA1_SIZE];

	spec_file_get_checked("TEST_COMMIT1_ID", &hex, NULL);

	ret = git_hex_to_sha1(hex, sha1);
	fail_unless(ret == 0, "git_hex_to_sha1(): %s\n", strerror(errno));
	free(hex);

	ret = git_commit_id(com_commit, com_sha1);
	fail_unless(ret == 0, "git_commit_id(): %s\n", strerror(errno));
	fail_unless(memcmp(sha1, com_sha1, GIT_SHA1_SIZE) == 0,
		    "sha1s doesn't match\n");
}
END_TEST

START_TEST(git_commit_tree_test)
{
	int ret;
	char *hex;
	unsigned char sha1[GIT_SHA1_SIZE];
	unsigned char tree_sha1[GIT_SHA1_SIZE];

	spec_file_get_checked("TEST_TREE1_ID", &hex, NULL);

	ret = git_hex_to_sha1(hex, sha1);
	fail_unless(ret == 0, "git_hex_to_sha1(): %s\n", strerror(errno));
	free(hex);

	ret = git_commit_tree(com_commit, tree_sha1);
	fail_unless(ret == 0, "git_commit_tree(): %s\n", strerror(errno));
	fail_unless(memcmp(sha1, tree_sha1, GIT_SHA1_SIZE) == 0,
		    "sha1s doesn't match\n");
}
END_TEST

START_TEST(git_commit_author_name_test)
{
	int ret;
	size_t len = 0;
	size_t alen = 0;
	char *author_name;
	const char *ret_author_name;

	spec_file_get_checked("TEST_USER1_NAME", &author_name, &len);

	len--; // Do not count the trailing '\0'

	ret = git_commit_author_name(com_commit, &ret_author_name, &alen);
	fail_unless(ret == 0, "git_commit_author_name(): %s\n", strerror(errno));
	fail_unless(ret_author_name != NULL, "ret_author_name is NULL\n");
	fail_unless(alen == len, "%d != %d\n", alen, len);
	fail_unless(!memcmp(author_name, ret_author_name, len),
		    "%s != %s\n", author_name, ret_author_name);

	free(author_name);
}
END_TEST

START_TEST(git_commit_author_email_test)
{
	int ret;
	size_t len = 0;
	size_t elen = 0;
	char *author_email;
	const char *ret_author_email;

	spec_file_get_checked("TEST_USER1_EMAIL", &author_email, &len);

	len--; // Do not count the trailing '\0'

	ret = git_commit_author_email(com_commit, &ret_author_email, &elen);
	fail_unless(ret == 0, "git_commit_author_email(): %s\n",
		    strerror(errno));
	fail_unless(ret_author_email != NULL, "ret_author_email is NULL\n");
	fail_unless(elen == len, "%d != %d\n", elen, len);
	fail_unless(!memcmp(author_email, ret_author_email, len),
		    "%s != %s\n", author_email, ret_author_email);

	free(author_email);
}
END_TEST

START_TEST(git_commit_author_date_test)
{
	int ret, com_tz;
	const int hcoded_tz = -200;
	time_t com_time;
	const time_t hcoded_time = 1165971600;

	/* FIXME: hardcoded data */

	ret = git_commit_author_date(com_commit, &com_time, &com_tz);
	fail_unless(ret == 0, "can't get author date: %d\n", ret);
	fail_unless(com_time == hcoded_time, "%d != %d\n", com_time,
		    hcoded_time);
	fail_unless(com_tz == hcoded_tz, "%d != %d\n", com_tz, hcoded_tz);
}
END_TEST

START_TEST(git_commit_committer_name_test)
{
	int ret;
	size_t len = 0;
	size_t nlen = 0;
	char *com_name;
	const char *ret_com_name;

	spec_file_get_checked("TEST_COMM1_NAME", &com_name, &len);

	len--; // Do not count the trailing '\0'

	ret = git_commit_committer_name(com_commit, &ret_com_name, &nlen);
	fail_unless(ret == 0, "git_commit_committer_name(): %s\n", strerror(errno));
	fail_unless(ret_com_name != NULL, "ret_com_name is NULL\n");
	fail_unless(nlen == len, "%d != %d\n", nlen, len);
	fail_unless(!memcmp(com_name, ret_com_name, len),
		    "%s != %s\n", com_name, ret_com_name);

	free(com_name);
}
END_TEST

START_TEST(git_commit_committer_email_test)
{
	int ret;
	size_t len = 0;
	size_t elen = 0;
	char *com_email;
	const char *ret_com_email;

	spec_file_get_checked("TEST_COMM1_EMAIL", &com_email, &len);

	len--; // Do not count the trailing '\0'

	ret = git_commit_committer_email(com_commit, &ret_com_email, &elen);
	fail_unless(ret == 0, "git_commit_committer_email(): %s\n", strerror(errno));
	fail_unless(ret_com_email != NULL, "ret_com_email is NULL\n");
	fail_unless(elen == len, "%d != %d\n", elen, len);
	fail_unless(!memcmp(com_email, ret_com_email, len),
		    "%s != %s\n", com_email, ret_com_email);

	free(com_email);
}
END_TEST

START_TEST(git_commit_committer_date_test)
{
	int ret, com_tz;
	const int hcoded_tz = -200;
	time_t com_time;
	const time_t hcoded_time = 1165971600;

	/* FIXME: hardcoded data */

	ret = git_commit_committer_date(com_commit, &com_time, &com_tz);
	fail_unless(ret == 0, "can't get author date: %d\n", ret);
	fail_unless(com_time == hcoded_time, "%d != %d\n", com_time,
		    hcoded_time);
	fail_unless(com_tz == hcoded_tz, "%d != %d\n", com_tz, hcoded_tz);
}
END_TEST

START_TEST(git_commit_message_test)
{
	int ret;
	size_t len = 0;
	size_t mlen = 0;
	char *msg;
	const char *ret_msg;

	spec_file_get_checked("TEST_FILE1_COMMIT", &msg, &len);

	ret = git_commit_message(com_commit, &ret_msg, &mlen);
	fail_unless(ret == 0, "git_commit_message(): %s\n", strerror(errno));
	fail_unless(ret_msg != NULL, "ret_msg is NULL\n");
	fail_unless(mlen == len, "%d != %d\n", mlen, len);


	// FIXME: We're not checking the trailing '\n' because
	// spec_file_get() doesn't return it
	fail_unless(!memcmp(msg, ret_msg, len-1), "%s != %s\n", msg, ret_msg);

	free(msg);
}
END_TEST

static struct git_commit *err_commit;

static void err_setup(void)
{
	err_commit = git_commit_init();
	fail_unless(err_commit != NULL, "git_commit_init() failed\n");
}

static void err_teardown(void)
{
	git_commit_free(err_commit);
}

static void commit_err_test(const char *(*commit_func)(struct git_commit *commit))
{
	const char *ret;

	fail_unless(commit_func != NULL, "commit_func() is NULL");

	ret = commit_func(NULL);
	fail_unless(ret == NULL, "didn't fail\n");
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);

	/* must survive */
	ret = commit_func(err_commit);
	fail_unless(ret == NULL, "didn't fail\n");
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);
}

START_TEST(git_commit_committer_date_err_test)
{
	time_t a;
	int ret, b;

	ret = git_commit_committer_date(NULL, NULL, NULL);
	fail_unless(ret == -1, "didn't fail\n");
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);

	/* must survive */
	ret = git_commit_committer_date(err_commit, &a, &b);
	fail_unless(ret == -1, "didn't fail\n");
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);
}
END_TEST

START_TEST(git_commit_author_date_err_test)
{
	time_t a;
	int ret, b;

	ret = git_commit_author_date(NULL, NULL, NULL);
	fail_unless(ret == -1, "didn't fail\n");
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);

	/* must survive */
	ret = git_commit_author_date(err_commit, &a, &b);
	fail_unless(ret == -1, "didn't fail\n");
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);
}
END_TEST

START_TEST(git_commit_raw_err_test)
{
	commit_err_test(git_commit_raw);
}
END_TEST

START_TEST(git_commit_lookup_twice_test)
{
	int err;
	struct git_commit *com1, *com2;
	unsigned char sha1[GIT_SHA1_SIZE];

	err = git_repo_open(git_repository_dir1);
	fail_unless(err == 0, "Can't open repository: %s\n", strerror(errno));

	err = git_repo_head(sha1);
	fail_unless(err == 0, "git_repo_head(): %s\n", strerror(errno));

	/*
	 * You should not get a double-free if you lookup
	 * the same commit object twice.
	 */
	com1 = git_commit_lookup(sha1);
	fail_unless(com1 != NULL, "git_commit_lookup(): %s\n",strerror(errno));

	com2 = git_commit_lookup(sha1);
	fail_unless(com2 != NULL, "git_commit_lookup(): %s\n",strerror(errno));

	git_commit_free(com1);
	git_commit_free(com2);
}
END_TEST

Suite *ltcommit_test_suite(void)
{
	Suite *s;
	TCase *tc_prim, *tc_com, *tc_err, *tc_reg;

	s = suite_create("ltcommit");

	tc_prim = tcase_create("Primitive operations");
	tcase_add_test(tc_prim, git_commit_init_test);
	tcase_add_test(tc_prim, git_commit_free_test);
	tcase_add_test(tc_prim, git_commit_to_str_test);
	suite_add_tcase(s, tc_prim);

	tc_com = tcase_create("Commit operations");
	tcase_add_checked_fixture(tc_com, com_setup, com_teardown);
	tcase_add_test(tc_com, git_commit_raw_test);
	tcase_add_test(tc_com, git_commit_id_test);
	tcase_add_test(tc_com, git_commit_tree_test);
	tcase_add_test(tc_com, git_commit_author_name_test);
	tcase_add_test(tc_com, git_commit_author_email_test);
	tcase_add_test(tc_com, git_commit_author_date_test);
	tcase_add_test(tc_com, git_commit_committer_name_test);
	tcase_add_test(tc_com, git_commit_committer_email_test);
	tcase_add_test(tc_com, git_commit_committer_date_test);
	tcase_add_test(tc_com, git_commit_message_test);
	suite_add_tcase(s, tc_com);

	tc_err = tcase_create("Common errors");
	tcase_add_checked_fixture(tc_err, err_setup, err_teardown);
	tcase_add_test(tc_err, git_commit_committer_date_err_test);
	tcase_add_test(tc_err, git_commit_author_date_err_test);
	tcase_add_test(tc_err, git_commit_raw_err_test);
	suite_add_tcase(s, tc_err);

	tc_reg = tcase_create("Bugs and regressions");
	tcase_add_test(tc_reg, git_commit_lookup_twice_test);
	suite_add_tcase(s, tc_reg);

	return s;
}
