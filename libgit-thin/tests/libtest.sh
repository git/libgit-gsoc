die_on_error()
{
	retcode=$1
	errmsg=$2

	if [ $retcode -ne 0 ]; then
		printf 'ERROR: %s\n' "$errmsg" >> /dev/stderr
		exit 1
	fi
}

set_commiter()
{
	name=$1
	email=$2

	export GIT_COMMITTER_NAME="$name"
	export GIT_COMMITTER_EMAIL="$email"
}

unset_commiter()
{
	unset GIT_COMMITTER_NAME GIT_COMMITTER_EMAIL
}

set_commit_date()
{
	date=$1

	export GIT_AUTHOR_DATE="$date"
	export GIT_COMMITTER_DATE="$date"
}

unset_commit_date()
{
	unset GIT_AUTHOR_DATE GIT_COMMITTER_DATE
}

config_add_user()
{
	user=$1
	email=$2

	git config --add user.name "$user"
	die_on_error $? "Can't add user: $user"

	git config --add user.email "$email"
	die_on_error $? "Can't add email: $email"
}

commit()
{
	msg=$1

	git-commit -a -m "$msg"
	die_on_error $? "can't commit"
}

add_new_file()
{
	contents=$1
	file=$2

	printf '%s' "$contents" > $file
	die_on_error $? "can't create new file $file"

	git-add $file
	die_on_error $? "can't add file $file"
}

add_contents()
{
	file=$1
	contents=$2

	printf '%s' "$contents" >> $file
}
