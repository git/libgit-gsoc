#include "tests.h"

START_TEST(git_revlist_init_test)
{
	struct git_revlist_opt *opt;

	opt = git_revlist_init();
	fail_unless(opt != NULL, "opt is NULL\n");

	free(opt);
}
END_TEST

START_TEST(git_revlist_free_test)
{
	struct git_revlist_opt *opt;

	opt = git_revlist_init();
	fail_unless(opt != NULL, "opt is NULL\n");

	git_revlist_free(opt);
}
END_TEST

START_TEST(git_revlist_one_commit_test)
{
	int ret;
	struct git_commit *commit;
	struct git_revlist_opt *opt;
	unsigned char sha1[GIT_HEX_LENGTH];

	ret = git_repo_open(git_repository_dir1);
	fail_unless(ret == 0, "Can't initialize repository [%d]\n", ret);

	ret = git_repo_head(sha1);
	fail_unless(ret == 0, "git_repo_head() failed: %s\n", strerror(errno));

	opt = git_revlist_init();
	fail_unless(opt != NULL, "opt is NULL\n");

	commit = git_commit_init();
	fail_unless(commit != NULL, "commit is NULL\n");

	ret = git_revlist_include(opt, sha1);
	fail_unless(ret == 0, "git_revlist_include() failed: %s\n",
		    strerror(errno));

	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 1, "No commit to get (ret is %d)\n", ret);

	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 0, "There should be only one commit [%d]\n", ret);

	git_revlist_free(opt);
	git_commit_free(commit);
}
END_TEST

START_TEST(git_revlist_three_commits_test)
{
	int ret;
	size_t len;
	char *msg_spec;
	const char *msg;
	struct git_commit *commit;
	struct git_revlist_opt *opt;
	unsigned char sha1[GIT_SHA1_SIZE];

	ret = git_repo_open(git_repository_dir2);
	fail_unless(ret == 0, "Can't initialize repository [%d]\n", ret);

	commit = git_commit_init();
	fail_unless(commit != NULL, "commit is NULL\n");

	opt = git_revlist_init();
	fail_unless(opt != NULL, "opt is NULL\n");

	ret = git_repo_head(sha1);
	fail_unless(ret == 0, "git_hex_to_sha1() failed: %s", strerror(errno));

	ret = git_revlist_include(opt, sha1);
	fail_unless(ret == 0, "git_revlist_include() failed: %d", ret);

	/*
	 * Get the top commit
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 1, "No commit to get (ret is 0)\n");

	spec_file_get_checked("TEST_FILE3_COMMIT3", &msg_spec, &len);
	ret = git_commit_message(commit, &msg, NULL);
	fail_unless(ret == 0, "git_commit_message(): %s\n", strerror(errno));
	/* FIXME: spec_file_get() doesn't the trailing '\n' */
	fail_unless(!memcmp(msg_spec, msg, len-1), "%s != %s\n", msg_spec, msg);
	free(msg_spec);

	/*
	 * Get the next commit
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 1, "No commit to get (ret is 0)\n");

	spec_file_get_checked("TEST_FILE2_COMMIT2", &msg_spec, &len);
	ret = git_commit_message(commit, &msg, NULL);
	fail_unless(ret == 0, "git_commit_message(): %s\n", strerror(errno));
	/* FIXME: spec_file_get() doesn't the trailing '\n' */
	fail_unless(!memcmp(msg_spec, msg, len-1), "%s != %s\n", msg_spec, msg);
	free(msg_spec);

	/*
	 * Get the last commit
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 1, "No commit to get (ret is 0)\n");

	spec_file_get_checked("TEST_FILE2_COMMIT1", &msg_spec, &len);
	ret = git_commit_message(commit, &msg, NULL);
	fail_unless(ret == 0, "git_commit_message(): %s\n", strerror(errno));
	/* FIXME: spec_file_get() doesn't the trailing '\n' */
	fail_unless(!memcmp(msg_spec, msg, len-1), "%s != %s\n", msg_spec, msg);
	free(msg_spec);

	/*
	 * The end...
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 0, "There should be only three commits [%d]\n",ret);

	git_commit_free(commit);
	git_revlist_free(opt);
}
END_TEST

START_TEST(git_revlist_exclude_test)
{
	int ret;
	size_t len;
	const char *msg;
	char *msg_spec, *hex_spec;
	struct git_commit *commit;
	struct git_revlist_opt *opt;
	unsigned char sha1[GIT_SHA1_SIZE];
	unsigned char sha2[GIT_SHA1_SIZE];

	ret = git_repo_open(git_repository_dir2);
	fail_unless(ret == 0, "Can't initialize repository [%d]\n", ret);

	commit = git_commit_init();
	fail_unless(commit != NULL, "commit is NULL\n");

	opt = git_revlist_init();
	fail_unless(opt != NULL, "opt is NULL\n");

	ret = git_repo_head(sha1);
	fail_unless(ret == 0, "git_hex_to_sha1() failed: %s", strerror(errno));

	ret = git_revlist_include(opt, sha1);
	fail_unless(ret == 0, "git_revlist_include() failed: %d", ret);

	spec_file_get_checked("TEST_FILE2_COMMIT_ID", &hex_spec, &len);
	ret = git_hex_to_sha1(hex_spec, sha2);
	fail_unless(ret == 0, "git_hex_to_sha1() failed: %d", ret);

	ret = git_revlist_exclude(opt, sha2);
	fail_unless(ret == 0, "git_revlist_exclude() failed: %d", ret);

	/*
	 * Get the top commit
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 1, "No commit to get (ret is 0)\n");

	spec_file_get_checked("TEST_FILE3_COMMIT3", &msg_spec, &len);
	ret = git_commit_message(commit, &msg, NULL);
	fail_unless(ret == 0, "git_commit_message(): %s\n", strerror(errno));
	/* FIXME: spec_file_get() doesn't the trailing '\n' */
	fail_unless(!memcmp(msg_spec, msg, len-1), "%s != %s\n", msg_spec, msg);
	free(msg_spec);

	/*
	 * The end...
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 0, "There should be only three commits [%d]\n",ret);

	git_commit_free(commit);
	git_revlist_free(opt);
}
END_TEST

START_TEST(git_revlist_reverse_test)
{
	int ret;
	size_t len;
	char *msg_spec;
	const char *msg;
	struct git_commit *commit;
	struct git_revlist_opt *opt;
	unsigned char sha1[GIT_SHA1_SIZE];

	ret = git_repo_open(git_repository_dir2);
	fail_unless(ret == 0, "Can't initialize repository [%d]\n", ret);

	commit = git_commit_init();
	fail_unless(commit != NULL, "commit is NULL\n");

	opt = git_revlist_init();
	fail_unless(opt != NULL, "opt is NULL\n");

	ret = git_repo_head(sha1);
	fail_unless(ret == 0, "git_hex_to_sha1() failed: %s", strerror(errno));

	ret = git_revlist_include(opt, sha1);
	fail_unless(ret == 0, "git_revlist_include() failed: %d", ret);

	ret = git_revlist_reverse(opt);
	fail_unless(ret == 0, "git_revlist_reverse() failed: %d", ret);

	/*
	 * Get the first commit
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 1, "No commit to get (ret is 0)\n");

	spec_file_get_checked("TEST_FILE2_COMMIT1", &msg_spec, &len);
	ret = git_commit_message(commit, &msg, NULL);
	fail_unless(ret == 0, "git_commit_message(): %s\n", strerror(errno));
	/* FIXME: spec_file_get() doesn't the trailing '\n' */
	fail_unless(!memcmp(msg_spec, msg, len-1), "%s != %s\n", msg_spec, msg);
	free(msg_spec);

	/*
	 * Get the second commit
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 1, "No commit to get (ret is 0)\n");

	spec_file_get_checked("TEST_FILE2_COMMIT2", &msg_spec, &len);
	ret = git_commit_message(commit, &msg, NULL);
	fail_unless(ret == 0, "git_commit_message(): %s\n", strerror(errno));
	/* FIXME: spec_file_get() doesn't the trailing '\n' */
	fail_unless(!memcmp(msg_spec, msg, len-1), "%s != %s\n", msg_spec, msg);
	free(msg_spec);

	/*
	 * Get the last commit
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 1, "No commit to get (ret is 0)\n");

	spec_file_get_checked("TEST_FILE3_COMMIT3", &msg_spec, &len);
	ret = git_commit_message(commit, &msg, NULL);
	fail_unless(ret == 0, "git_commit_message(): %s\n", strerror(errno));
	/* FIXME: spec_file_get() doesn't the trailing '\n' */
	fail_unless(!memcmp(msg_spec, msg, len-1), "%s != %s\n", msg_spec, msg);
	free(msg_spec);

	/*
	 * The end...
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 0, "There should be only three commits [%d]\n",ret);

	git_commit_free(commit);
	git_revlist_free(opt);
}
END_TEST

START_TEST(git_revlist_max_count_test)
{
	int ret;
	size_t len;
	char *msg_spec;
	const char *msg;
	struct git_commit *commit;
	struct git_revlist_opt *opt;
	unsigned char sha1[GIT_SHA1_SIZE];

	ret = git_repo_open(git_repository_dir2);
	fail_unless(ret == 0, "Can't initialize repository [%d]\n", ret);

	commit = git_commit_init();
	fail_unless(commit != NULL, "commit is NULL\n");

	opt = git_revlist_init();
	fail_unless(opt != NULL, "opt is NULL\n");

	ret = git_repo_head(sha1);
	fail_unless(ret == 0, "git_hex_to_sha1() failed: %s", strerror(errno));

	ret = git_revlist_include(opt, sha1);
	fail_unless(ret == 0, "git_revlist_include() failed: %d", ret);

	ret = git_revlist_max_count(opt, 2);
	fail_unless(ret == 0, "git_revlist_max_count() failed: %d", ret);

	/*
	 * Get the top commit
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 1, "No commit to get (ret is 0)\n");

	spec_file_get_checked("TEST_FILE3_COMMIT3", &msg_spec, &len);
	ret = git_commit_message(commit, &msg, NULL);
	fail_unless(ret == 0, "git_commit_message(): %s\n", strerror(errno));
	/* FIXME: spec_file_get() doesn't the trailing '\n' */
	fail_unless(!memcmp(msg_spec, msg, len-1), "%s != %s\n", msg_spec, msg);
	free(msg_spec);

	/*
	 * Get the next commit
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 1, "No commit to get (ret is 0)\n");

	spec_file_get_checked("TEST_FILE2_COMMIT2", &msg_spec, &len);
	ret = git_commit_message(commit, &msg, NULL);
	fail_unless(ret == 0, "git_commit_message(): %s\n", strerror(errno));
	/* FIXME: spec_file_get() doesn't the trailing '\n' */
	fail_unless(!memcmp(msg_spec, msg, len-1), "%s != %s\n", msg_spec, msg);
	free(msg_spec);

	/*
	 * The end...
	 */
	ret = git_revlist_next(opt, commit);
	fail_unless(ret == 0, "There should be only three commits [%d]\n",ret);

	git_commit_free(commit);
	git_revlist_free(opt);
}
END_TEST

START_TEST(git_revlist_next_err_test)
{
	int err;
	struct git_commit *commit;
	struct git_revlist_opt *opt;

	err = git_revlist_next(NULL, NULL);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);

	err = git_revlist_next(opt, NULL);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);

	err = git_revlist_next(NULL, commit);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);
}
END_TEST

START_TEST(git_revlist_include_err_test)
{
	int err;
	unsigned char sha1[1];
	struct git_revlist_opt *opt;

	err = git_revlist_include(NULL, NULL);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);

	err = git_revlist_include(opt, NULL);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);

	err = git_revlist_include(NULL, sha1);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);
}
END_TEST

Suite *ltrevlist_test_suite(void)
{
	Suite *s;
	TCase *tc_prim, *tc_revlist, *tc_err;

	s = suite_create("ltrevlist");

	tc_prim = tcase_create("Primitive operations");
	tcase_add_test(tc_prim, git_revlist_init_test);
	tcase_add_test(tc_prim, git_revlist_free_test);
	suite_add_tcase(s, tc_prim);

	tc_revlist = tcase_create("Revision listing");
	tcase_add_test(tc_revlist, git_revlist_one_commit_test);
	tcase_add_test(tc_revlist, git_revlist_three_commits_test);
	tcase_add_test(tc_revlist, git_revlist_exclude_test);
	tcase_add_test(tc_revlist, git_revlist_reverse_test);
	tcase_add_test(tc_revlist, git_revlist_max_count_test);
	suite_add_tcase(s, tc_revlist);

	tc_err = tcase_create("Common errors");
	tcase_add_test(tc_err, git_revlist_next_err_test);
	tcase_add_test(tc_err, git_revlist_include_err_test);
	suite_add_tcase(s, tc_err);

	return s;
}
