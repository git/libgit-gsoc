#include "tests.h"

START_TEST(git_repo_open_test)
{
	int err;

	err = git_repo_open(git_repository_dir1);
	fail_unless(err == 0, "Can't initialize repository [%d]\n", err);
}
END_TEST

static void obj_setup(void)
{
	int ret;

	ret = git_repo_open(git_repository_dir1);
	fail_unless(ret == 0, "git_repo_open(): %s\n", strerror(errno));
}

START_TEST(git_repo_blob_read_test)
{
	int ret;
	void *blob;
	size_t clen, hlen, size;
	char *contents, *hex;
	unsigned char sha1[GIT_SHA1_SIZE];

	spec_file_get_checked("TEST_FILE1_CONTENTS", &contents, &clen);
	spec_file_get_checked("TEST_FILE1_BLOB_SHA1", &hex, &hlen);

	ret = git_hex_to_sha1(hex, sha1);
	fail_unless(ret == 0, "git_hex_to_sha1() %s\n", strerror(errno));
	free(hex);

	ret = git_repo_blob_read(sha1, &blob, &size);
	fail_unless(ret == 0, "git_repo_blob_read() %s\n", strerror(errno));
	fail_unless(blob != NULL, "blob is NULL\n");
	fail_unless(size > 0, "size is %d\n", (int) size);
	fail_unless(memcmp(blob, contents, clen) == 0, "%s != %s\n",
		    blob, contents);

	free(blob);
	free(contents);
}
END_TEST

START_TEST(git_repo_commit_read_test)
{
	int ret;
	void *buf;
	size_t size;
	char *tree_id, *msg, *hex;
	unsigned char sha1[GIT_SHA1_SIZE];

	spec_file_get_checked("TEST_TREE1_ID", &tree_id, NULL);
	spec_file_get_checked("TEST_FILE1_COMMIT", &msg, NULL);
	spec_file_get_checked("TEST_COMMIT1_ID", &hex, NULL);

	ret = git_hex_to_sha1(hex, sha1);
	fail_unless(ret == 0, "git_hex_to_sha1() %s\n", strerror(errno));
	free(hex);

	ret = git_repo_commit_read(sha1, &buf, &size);
	fail_unless(ret == 0, "git_repo_commit_read() %s\n", strerror(errno));
	fail_unless(buf != NULL, "buf is NULL\n");
	fail_unless(size > 0, "size is %d\n", (int) size);

	/*
	 * Only do basic checking, we don't care if it's a fake commit
	 * object for example.
	 */
	fail_unless(strstr(buf, tree_id) != NULL, "can't find %s in %s\n",
		    tree_id, (char *) buf);
	fail_unless(strstr(buf, msg) != NULL, "can't find %s in %s\n", msg,
		    (char *) buf);

	free(buf);
	free(msg);
	free(tree_id);
}
END_TEST

START_TEST(git_repo_tree_read_test)
{
	int ret;
	void *buf;
	size_t size;
	char *file, *hex;
	unsigned char sha1[GIT_SHA1_SIZE];

	spec_file_get_checked("TEST_FILE1", &file, NULL);
	spec_file_get_checked("TEST_TREE1_ID", &hex, NULL);

	ret = git_hex_to_sha1(hex, sha1);
	fail_unless(ret == 0, "git_hex_to_sha1() %s\n", strerror(errno));
	free(hex);

	ret = git_repo_tree_read(sha1, &buf, &size);
	fail_unless(ret == 0, "git_repo_tree_read() %s\n", strerror(errno));
	fail_unless(buf != NULL, "buf is NULL\n");
	fail_unless(size > 0, "size is %d\n", (int) size);

	/* FIXME: weak check */
	fail_unless(strstr(buf, file) != NULL, "can't find %s in %s\n", file,
		    (char *) buf);

	free(buf);
	free(file);
}
END_TEST

static char *com_id;

static void trans_setup(void)
{
	int ret;

	ret = git_repo_open(git_repository_dir1);
	fail_unless(ret == 0, "git_repo_open(): %s\n", strerror(errno));

	spec_file_get_checked("TEST_COMMIT1_ID", &com_id, NULL);
}

static void trans_teardown(void)
{
	free(com_id);
}

START_TEST(git_repo_translate_head_test)
{
	int ret;
	char hex[GIT_HEX_LENGTH + 1];
	unsigned char sha1[GIT_SHA1_SIZE];

	ret = git_repo_translate_ref("HEAD", sha1);
	fail_unless(ret == 0, "git_translate_ref() failed: %s\n",
		    strerror(errno));

	memset(hex, 0, GIT_HEX_LENGTH + 1);
	ret = git_sha1_to_hex(sha1, hex);
	fail_unless(ret == 0, "git_sha1_to_hex() failed: %s\n",
		    strerror(errno));

	fail_unless(memcmp(com_id, hex, GIT_HEX_LENGTH + 1) == 0,
		    "%s != %s\n", com_id, hex);
}
END_TEST

START_TEST(git_repo_translate_master_test)
{
	int ret;
	char hex[GIT_HEX_LENGTH + 1];
	unsigned char sha1[GIT_SHA1_SIZE];

	ret = git_repo_translate_ref("master", sha1);
	fail_unless(ret == 0, "git_translate_ref() failed: %s\n",
		    strerror(errno));

	memset(hex, 0, GIT_HEX_LENGTH + 1);
	ret = git_sha1_to_hex(sha1, hex);
	fail_unless(ret == 0, "git_sha1_to_hex() failed: %s\n",
		    strerror(errno));

	fail_unless(memcmp(com_id, hex, GIT_HEX_LENGTH + 1) == 0,
		    "%s != %s\n", com_id, hex);
}
END_TEST

START_TEST(git_repo_translate_tag_test)
{
	int ret;
	char hex[GIT_HEX_LENGTH + 1], *tag;
	unsigned char sha1[GIT_SHA1_SIZE];

	spec_file_get_checked("TEST_TAG1_NAME", &tag, NULL);

	ret = git_repo_translate_ref(tag, sha1);
	fail_unless(ret == 0, "git_translate_ref() failed: %s\n",
		    strerror(errno));

	memset(hex, 0, GIT_HEX_LENGTH + 1);
	ret = git_sha1_to_hex(sha1, hex);
	fail_unless(ret == 0, "git_sha1_to_hex() failed: %s\n",
		    strerror(errno));

	fail_unless(memcmp(com_id, hex, GIT_HEX_LENGTH + 1) == 0,
		    "%s != %s\n", com_id, hex);

	free(tag);
}
END_TEST

START_TEST(git_repo_head_test)
{
	int ret;
	char hex[GIT_HEX_LENGTH + 1];
	unsigned char sha1[GIT_SHA1_SIZE];

	ret = git_repo_head(sha1);
	fail_unless(ret == 0, "git_repo_head() failed: %s\n", strerror(errno));

	ret = git_sha1_to_hex(sha1, hex);
	fail_unless(ret == 0, "git_sha1_to_hex() failed: %s\n",
		    strerror(errno));

	fail_unless(memcmp(com_id, hex, GIT_HEX_LENGTH + 1) == 0,
		    "%s != %s\n", com_id, hex);
}
END_TEST

START_TEST(git_repo_open_err_test)
{
	int err;

	err = git_repo_open(NULL);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);

	err = git_repo_open("akjsndkanskj");
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == ENOENT, "errno doesn't match: %d\n", errno);
}
END_TEST

START_TEST(git_repo_translate_ref_err_test)
{
	int err;
	const char *ref;
	unsigned char *sha1;

	err = git_repo_translate_ref(NULL, NULL);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);

	err = git_repo_translate_ref(ref, NULL);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);

	err = git_repo_translate_ref(NULL, sha1);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);
}
END_TEST

START_TEST(git_repo_head_err_test)
{
	int err;

	err = git_repo_head(NULL);
	fail_unless(err == -1, "didn't fail: %d\n", err);
	fail_unless(errno == EINVAL, "errno doesn't match: %d\n", errno);
}
END_TEST

Suite *ltrepo_test_suite(void)
{
	Suite *s;
	TCase *tc_prim, *tc_obj, *tc_err, *tc_trans;

	s = suite_create("ltrepo");

	tc_prim = tcase_create("Primitive operations");
	tcase_add_test(tc_prim, git_repo_open_test);
	suite_add_tcase(s, tc_prim);

	tc_trans = tcase_create("Translate");
	tcase_add_checked_fixture(tc_trans, trans_setup, trans_teardown);
	tcase_add_test(tc_trans, git_repo_translate_head_test);
	tcase_add_test(tc_trans, git_repo_translate_master_test);
	tcase_add_test(tc_trans, git_repo_translate_tag_test);
	tcase_add_test(tc_trans, git_repo_head_test);
	suite_add_tcase(s, tc_trans);

	tc_obj = tcase_create("Read object");
	tcase_add_checked_fixture(tc_obj, obj_setup, NULL);
	tcase_add_test(tc_obj, git_repo_blob_read_test);
	tcase_add_test(tc_obj, git_repo_commit_read_test);
	tcase_add_test(tc_obj, git_repo_tree_read_test);
	suite_add_tcase(s, tc_obj);

	tc_err = tcase_create("Common errors");
	tcase_add_test(tc_err, git_repo_open_err_test);
	tcase_add_test(tc_err, git_repo_head_err_test);
	tcase_add_test(tc_err, git_repo_translate_ref_err_test);
	suite_add_tcase(s, tc_err);

	return s;
}
