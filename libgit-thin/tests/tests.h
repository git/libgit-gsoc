#ifndef TESTS_H
#define TESTS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <check.h>
#include <libgit-thin.h>

#include "misc.h"

extern const char *git_repository_dir1;
extern const char *git_repository_dir2;

Suite *ltsha1_test_suite(void);
Suite *ltrepo_test_suite(void);
Suite *ltcommit_test_suite(void);
Suite *ltrevlist_test_suite(void);

#endif /* TESTS_H */
