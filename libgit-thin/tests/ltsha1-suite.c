#include "tests.h"

START_TEST(git_sha1_conversion_test)
{
	int ret;
	char hex[GIT_HEX_LENGTH];
	unsigned char sha1[GIT_SHA1_SIZE];
	const char orig_hex[] = "190045d53b9a8341e8600d6eb468b6081e903afb";

	ret = git_hex_to_sha1(orig_hex, &sha1[0]);
	fail_unless(ret == 0, "git_hex_to_sha1() failed: %d\n", ret);

	ret = git_sha1_to_hex(sha1, hex);
	fail_unless(ret == 0, "git_sha1_to_hex() failed: %d\n", ret);

	fail_unless(memcmp(orig_hex, hex, GIT_HEX_LENGTH + 1) == 0,
		    "%s != %s\n", orig_hex, hex);
}
END_TEST

START_TEST(git_hex_to_sha1_err_test)
{
	int ret;
	const char *hex;
	unsigned char sha1[1];

	ret = git_hex_to_sha1(NULL, NULL);
	fail_unless(ret == -1, "git_hex_to_sha1() not failed: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is not EINVAL\n", errno);

	ret = git_hex_to_sha1(hex, NULL);
	fail_unless(ret == -1, "git_hex_to_sha1() not failed: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is not EINVAL\n", errno);

	ret = git_hex_to_sha1(NULL, sha1);
	fail_unless(ret == -1, "git_hex_to_sha1() not failed: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is not EINVAL\n", errno);
}
END_TEST

START_TEST(git_sha1_to_hex_err_test)
{
	int ret;
	char hex[1];
	unsigned char sha1[1];

	ret = git_sha1_to_hex(NULL, NULL);
	fail_unless(ret == -1, "git_hex_to_sha1() not failed: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is not EINVAL\n", errno);

	ret = git_sha1_to_hex(sha1, NULL);
	fail_unless(ret == -1, "git_hex_to_sha1() not failed: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is not EINVAL\n", errno);

	ret = git_sha1_to_hex(NULL, hex);
	fail_unless(ret == -1, "git_hex_to_sha1() not failed: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is not EINVAL\n", errno);
}
END_TEST

Suite *ltsha1_test_suite(void)
{
	Suite *s;
	TCase *tc_conv, *tc_err;

	s = suite_create("ltsha1");

	tc_conv = tcase_create("Conversion");
	tcase_add_test(tc_conv, git_sha1_conversion_test);
	suite_add_tcase(s, tc_conv);

	tc_err = tcase_create("Common errors");
	tcase_add_test(tc_err, git_hex_to_sha1_err_test);
	tcase_add_test(tc_err, git_sha1_to_hex_err_test);
	suite_add_tcase(s, tc_err);

	return s;
}
