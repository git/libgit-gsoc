/**
 * @file
 * 
 * Revision listing functions.
 *
 * The functions in this file implements revision listing in GIT
 * repositories.
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <cache.h>
#include <diff.h>
#include <commit.h>
#include <revision.h>

#include "ltsha1.h"
#include "ltcommit.h"
#include "ltrevlist.h"

struct hex_name {
	struct hex_name *next;
	char name[FLEX_ARRAY];
};

/**< Revsion listing options */
struct git_revlist_opt {
	int prepare;
	struct rev_info info;
	struct hex_name *hex_head;
	struct commit_list *list;
};

static void revlist_clear_commit_marks(struct git_revlist_opt *opt)
{
	struct commit_list *list;

	for (list = opt->list; list; list = list->next)
		clear_commit_marks(list->item, -1);
}

/**
 * Get the next commit object in the revision listing.
 * 
 * \param opt git_revlist_opt structure which describe the rules
 *            of this revision listing
 * \param commit git_commit structure to return the commit info
 *               into
 *
 * \return 1 on success (a commit have been retrieved), 0 on
 * end-of-commit-list and -1 on error.
 */
int git_revlist_next(struct git_revlist_opt *opt, struct git_commit *commit)
{
	struct commit *p;

	if (!opt || !commit) {
		errno = EINVAL;
		return -1;
	}

	if (!opt->prepare) {
		int err;

		err = prepare_revision_walk(&opt->info);
		if (err)
			return -1;

		opt->prepare = 1;
	}

	p = get_revision(&opt->info);
	if (!p)
		return 0;

	if (__git_commit_obj(commit)) {
		__git_commit_free(commit);
		__git_commit_init(commit);
	}

	__git_commit_obj_set(commit, p);
	return 1;
}

/**
 * Free all the memory allocated by a git_revlist_opt structure.
 * 
 * \param opt git_revlist_opt structure to be freed
 */
void git_revlist_free(struct git_revlist_opt *opt)
{
	struct hex_name *p;

	if (!opt)
		return;

	revlist_clear_commit_marks(opt);
	free_commit_list(opt->list);

	for (p = opt->hex_head; p;) {
		struct hex_name *n = p->next;
		free(p);
		p = n;
	}

	free(opt);
}

/**
 * Allocate and initialize a git_revlist_opt structure.
 * 
 * Default revision listing options:
 * 
 * 	- Don't show merges
 * 	- Descending chronological order
 * 
 * \return A pointer to an allocated git_revlist_opt structure
 * on success, NULL otherwise.
 */
struct git_revlist_opt *git_revlist_init(void)
{
	struct git_revlist_opt *opt;

	opt = malloc(sizeof(*opt));
	if (!opt)
		return NULL;

	init_revisions(&opt->info, NULL);

	/* defaults */
	opt->prepare = 0;
	opt->info.no_merges = 1;
	opt->hex_head = NULL;
	opt->list = NULL;

	return opt;
}

static int __revlist_add_commit(struct git_revlist_opt *opt,
				unsigned char *sha1, int exclude)
{
	int err;
	struct hex_name *new;
	struct commit *commit;

	if (!opt || !sha1) {
		errno = EINVAL;
		return -1;
	}

	commit = lookup_commit_reference_gently(sha1, 1);
	if (!commit)
		return -1;

	new = malloc(sizeof(*new) + GIT_HEX_LENGTH + 1);
	if (!new)
		return -1;

	err = git_sha1_to_hex(sha1, new->name);
	if (err) {
		free(new);
		return -1;
	}

	new->next = opt->hex_head;
	opt->hex_head = new;

	if (exclude)
		commit->object.flags |= UNINTERESTING;

	commit_list_insert(commit, &opt->list);

	add_pending_object(&opt->info, &commit->object, new->name);
	return 0;
}

/**
 * Include a commit into the revision listing.
 * 
 * Should be called before using git_revlist_next().
 * 
 * \param opt git_revlist_opt structure to include the commit into
 * \param sha1 commit's 20-byte SHA1 array
 * 
 * \return 0 on success, -1 otherwise.
 */
int git_revlist_include(struct git_revlist_opt *opt, unsigned char *sha1)
{
	return __revlist_add_commit(opt, sha1, 0);
}

/**
 * Exclude a commit into the revision listing.
 * 
 * Should be called before using git_revlist_next().
 * 
 * \param opt git_revlist_opt structure to exclude the commit from
 * \param sha1 commit's 20-byte SHA1 array
 * 
 * \return 0 on success, -1 otherwise.
 */
int git_revlist_exclude(struct git_revlist_opt *opt, unsigned char *sha1)
{
	return __revlist_add_commit(opt, sha1, 1);
}

/**
 * List commits in reverse order.
 * 
 * By default commits are listed in descending chronological order,
 * this function changes that behaivor and list commits in ascending
 * chronological order.
 * 
 * \param opt git_revlist_opt structure to set this option
 *
 * \return 0 on success, -1 otherwise.
 */
int git_revlist_reverse(struct git_revlist_opt *opt)
{
	if (!opt) {
		errno = EINVAL;
		return -1;
	}

	opt->info.reverse ^= 1;
	return 0;
}

/**
 * Show merges.
 * 
 * By default merge commits are not shown, this function changes that
 * behaivor and list merge commits as well.
 * 
 * \param opt git_revlist_opt structure to set this option
 *
 * \return 0 on success, -1 otherwise.
 */
int git_revlist_show_merges(struct git_revlist_opt *opt)
{
	if (!opt) {
		errno = EINVAL;
		return -1;
	}

	opt->info.no_merges = 0;
	return 0;
}

/**
 * Specify the number of commits to list.
 * 
 * By default all the reacheable commits will be listed, this
 * option changes that behaivor and allows the caller to specify
 * the number of commits to be listed.
 * 
 * \param opt git_revlist_opt structure to set this option
 * \param count number of commits to be listed
 *
 * \return 0 on success, -1 otherwise.
 */
int git_revlist_max_count(struct git_revlist_opt *opt, size_t count)
{
	if (!opt) {
		errno = EINVAL;
		return -1;
	}

	opt->info.max_count = (int) count;
	return 0;
}
