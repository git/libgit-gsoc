It seems that libcheck doesn't support variable sharing between tests.
For example, say you have:

"""
static struct *foo;

START_TEST(test1)
{
      foo = malloc(sizeof(*foo);
      foo->heh = 1;
}
END_TEST

START_TEST(test2)
{
    fain_unless(foo->heh == 1);
}
END_TEST
"""

You'll get a segfault in test2, because foo will be NULL there. What happens
here is that libcheck forks each test and run them into a separate process.

This' a feature because you can catch segfaults reabily and because it forces
you to write self-contained tests.

But the problem for us is that we can't implement small tests, like this:

test1: revlist setup
test2: get the top commit
test3: get the next commit
test4: get the last commit
test5: end revlist

Because we'd need to share variables among tests...

I don't know what to do to fix this... And it's something important because we
need to perform this kind of test.
