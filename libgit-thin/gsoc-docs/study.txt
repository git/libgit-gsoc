1 Objects
---------

 All the information tracked by git is stored and organized in the form
of objects.

 Objects are stored in the object store, which is literally just a
content-addressable collection of objects. All objects are named by their
content, which is approximated by the SHA1 hash of the object itself.

 Git recognizes the following objects types:

 o Blob

 Pure storage object, contains user data. Cannot refer to any other
 object.

 Is created with git-update-index (or git-add) and its contents can be
 accessed with git-cat-file.

 o Tree

 Ties one or more objects into a directory structure. Can refer to other
 tree objects, thus creating a directory hierarchy.

 Is a list of mode/name/blob data, sorted by name. The mode data may
 specify a directory mode, in which case instead of naming a blob, that name
 is associated with another tree object.

 A tree is created with git-write-tree, its data can be accessed with
 git-ls-tree an two trees can be compared with git-diff-tree.

 o Commit

 Ties such directory hierarchy together into a DAG of revisions.

 Each commit is associated exactly with one tree (the directory hierarchy
 at the time of the commit). In addition, a commit refers to one or more
 parent commit objects that describe the history of how we arrived at that
 directory hierarchy.

 The root commit is the first commit of a project.

 The SHA1 signature of a commit refers to the SHA1 signatures of the tree
 it is associated with and the signatures of the parent, a single named
 commit specifies uniquely a whole set of history, with full contents.
 You can't later fake any step of the way once you have the name of a commit.

 A commit is created with git-commit-tree and  its data can be accessed
 by git-cat-file.

 o Tag

 Symbolically identifies and can be used to sign other objects.

 The following picture shows the relationship between objects:


      (root)   parent          parent       
      commit <------- commit <------- commit <------- ....
        |               |               |
        |               |               |
        |               |               |
      tree            tree            tree
        |               |               |
        |               |               .
        |              / \              .
      blobs           /   \
                     /     \       (imagine a more
                   tree    tree     complex hierarchy)
                    .       .
                    .       .
                    .       .
                   blobs   blobs

2 Index
-------

 Stores the current working tree state. It's used to checkout files and
make comparisons efficiently.

3 Workflow
----------

 o working directory -> index

   The index can be updated with the information from the working
   directory with the git-update-index program.

 o index -> object database

   You write your current index file to a tree object with the
   git-write-tree program.

   It returns the name of the resulting top-level tree.

 o object database -> index

   You read a tree file from the object database, and use that to
   populate the current index with git-read-tree program.

 o index -> working directory

   To check out files from the index use the git-checkout-index program.
