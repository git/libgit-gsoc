AR = ar
CC = gcc
INSTALL = install
CFLAGS = -O0 -Wall -W -Wmissing-declarations -Wmissing-prototypes \
	 -Wshadow -Wbad-function-cast -Wcast-qual
GIT_SRC_DIR = ..
GIT_ADD_HEADERS = -DSHA1_HEADER=\<openssl/sha.h\>
DOXYGEN_CONFIG = doxygen.conf
DOXYGEN_DIR = Documentation
DESTDIR := $(HOME)/libgit-thin

export CC CFLAGS

LIB_ARCHIVE = libgit-thin.a
LIB_OBJ = ltcommit.o ltrevlist.o ltrepo.o ltsha1.o
LIB_H = libgit-thin.h ltcommit.h ltrepo.h ltrevlist.h ltsha1.h

QUIET_SUBDIR0  = +$(MAKE) -C # space to separate -C and subdir
QUIET_SUBDIR1  =

ifneq ($(findstring $(MAKEFLAGS),s),s)
ifndef V
	QUIET_CC       = @echo '   ' CC $@;
	QUIET_AR       = @echo '   ' AR $@;
	QUIET_SUBDIR0  = +@subdir=
	QUIET_SUBDIR1  = ;$(NO_SUBDIR) echo '   ' SUBDIR $$subdir; \
			 $(MAKE) $(PRINT_DIR) -C $$subdir
	export V
endif
endif

all: $(LIB_ARCHIVE) tests examples

$(LIB_ARCHIVE): $(LIB_OBJ) libgit-thin.h
	$(QUIET_AR)rm -f $@ && $(AR) rcs $@ $(LIB_OBJ)

ltcommit.o: ltcommit.c ltcommit.h
	$(QUIET_CC)$(CC) -c $< -I$(GIT_SRC_DIR) $(GIT_ADD_HEADERS) $(CFLAGS)

ltrevlist.o: ltrevlist.c ltrevlist.h ltcommit.o
	$(QUIET_CC)$(CC) -c $< -I$(GIT_SRC_DIR) $(GIT_ADD_HEADERS) $(CFLAGS)

ltrepo.o: ltrepo.c ltrepo.h
	$(QUIET_CC)$(CC) -c $< -I$(GIT_SRC_DIR) $(GIT_ADD_HEADERS) $(CFLAGS)

ltsha1.o: ltsha1.c ltsha1.h
	$(QUIET_CC)$(CC) -c $< -I$(GIT_SRC_DIR) $(GIT_ADD_HEADERS) $(CFLAGS)

.PHONY: pygit doc clean tests test check examples

pygit: $(LIB_ARCHIVE)
	$(QUIET_SUBDIR0)pygit $(QUIET_SUBDIR1)

tests: $(LIB_ARCHIVE)
	$(QUIET_SUBDIR0)tests $(QUIET_SUBDIR1)

examples: $(LIB_ARCHIVE)
	$(QUIET_SUBDIR0)examples $(QUIET_SUBDIR1)

test check: tests
	$(QUIET_SUBDIR0)tests $(QUIET_SUBDIR1) check

doc:
	@doxygen $(DOXYGEN_CONFIG)

# FIXME: We should not copy GIT's files...
install: all
	$(INSTALL) -d $(DESTDIR)
	$(INSTALL) -m 644 libgit-thin.a $(DESTDIR)
	$(INSTALL) -m 644 $(LIB_H) $(DESTDIR)
	$(INSTALL) -m 644 $(GIT_SRC_DIR)/libgit.a $(DESTDIR)
	$(INSTALL) -m 644 $(GIT_SRC_DIR)/xdiff/lib.a $(DESTDIR)/libxdiff.a

clean:
	rm -f $(PROGRAMS) *.o *~ *.a core.*
	$(MAKE) -C tests clean
	$(MAKE) -C examples clean
	rm -rf $(DOXYGEN_DIR)
