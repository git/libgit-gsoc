/**
 * @file
 * 
 * Commit handling functions.
 *
 * The functions implemented in this file retrieve all kinds of
 * information from commits.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <cache.h>
#include <commit.h>

#include "ltcommit.h"

/**< Commit structure */
struct git_commit {
	struct commit *commit;
	int author_date;
	int author_tz;
	time_t author_time;
	int committer_date;
	int committer_tz;
	time_t committer_time;
	int tree_id;
	unsigned char tree_sha1[20];
};

static char *commit_strstr(const struct git_commit *commit,
			   const char *needle)
{
	char *p, *endp;

	if (!commit->commit)
		return NULL;

	p = strstr(commit->commit->buffer, needle);
	if (!p)
		return NULL;

	endp = strstr(commit->commit->buffer, "\n\n");
	if (!endp)
		endp = commit->commit->buffer + strlen(commit->commit->buffer);

	return (p < endp) ? p : NULL;
}

static char *email_line_strchr(char *p, int c)
{
	while (*p && *p != c && *p != '\n')
		p++;

	return (*p != c) ? NULL : p;
}

static inline char *email_line_start(char *p)
{
	return email_line_strchr(p, '<');
}

static inline char *email_line_end(char *p)
{
	return email_line_strchr(p, '>');
}

static inline char *tz_start(char *p)
{
	return email_line_strchr(p, ' ');
}

static inline int check_strtoul_ret(unsigned long ret)
{
	if ((!ret || ret == ULONG_MAX) && errno != 0)
		return -1;
	return 0;
}

static inline int check_strtol_ret(long ret)
{
	if ((ret == LONG_MIN || ret == LONG_MAX) && errno != 0)
		return -1;
	return 0;
}

/**
 * Get the commit's message.
 * 
 * \param commit git_commit structure to get the message from
 * \param ret pointer to return the commit message
 * \param len message's length
 * 
 * \return 0 on success, -1 otherwise
 */
int git_commit_message(struct git_commit *commit,
		       const char **ret, size_t *len)
{
	char *p;

	if (!commit || !commit->commit || !ret)
		goto out_err;

	p = strstr(commit->commit->buffer, "\n\n");
	if (!p) {
		/* empty message (probably) */
		errno = 0;
		return -1;
	}

	p += 2;
	if (!p)
		goto out_err;

	*ret = p;
	if (len)
		*len = strlen(p);

	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

/**
 * Get the commit's committer email.
 * 
 * \param commit git_commit structure to get the committer's email
 *               from
 * \param ret pointer to return the committer's email into
 * \param len committer's email length
 * 
 * \return 0 on success, -1 otherwise
 */
int git_commit_committer_email(struct git_commit *commit,
			       const char **ret, size_t *len)
{
	char *p, *endp;

	if (!commit || !ret)
		goto out_err;

	p = commit_strstr(commit, "committer ");
	if (!p)
		goto out_err;

	p = email_line_start(p);
	if (!p)
		goto out_err;

	endp = email_line_end(p);
	if (!endp)
		goto out_err;

	*ret = ++p;
	if (len)
		*len = endp - p;

	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

/**
 * Get the commit's committer name.
 * 
 * \param commit git_commit structure to get the committer's name
 *               from
 * \param ret pointer to return the committer's name into
 * \param len committer's name length
 * 
 * \return 0 on success, -1 otherwise
 */
int git_commit_committer_name(struct git_commit *commit,
			      const char **ret, size_t *len)
{
	char *p, *endp;

	if (!commit || !ret)
		goto out_err;

	p = commit_strstr(commit, "committer ");
	if (!p)
		goto out_err;

	endp = email_line_start(p);
	if (!endp)
		goto out_err;

	--endp;
	if (*endp != ' ')
		goto out_err;

	p += 10;

	*ret = p;
	if (len)
		*len = endp - p;

	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

/**
 * Get the commit's (committer) date
 * 
 * \param commit git_commit structure to get the (committer's) date from
 * \param com_time return the date timestamp as seconds since the
 *        epoch, UTC (might be NULL)
 * \param com_tz return the timezone information as the number of
 *               hours and minutes offset from UTC (might be NULL)
 * 
 * \return 0 on succes, -1 otherwise
 */
int git_commit_committer_date(struct git_commit *commit,
			      time_t *com_time, int *com_tz)
{
	if (!commit)
		goto out_err;

	if (!com_time && !com_tz)
		goto out_err;

	if (!commit->committer_date) {
		int err;
		char *p;

		p = commit_strstr(commit, "committer ");
		if (!p)
			goto out_err;

		p = email_line_end(p);
		if (!p)
			goto out_err;

		++p;
		if (*p != ' ')
			goto out_err;

		errno = 0;
		commit->committer_time = (time_t) strtoul(++p, NULL, 10);
		err = check_strtoul_ret(commit->committer_time);
		if (err)
			return -1;

		p = tz_start(p);
		if (!p)
			goto out_err;

		errno = 0;
		commit->committer_tz = strtol(++p, NULL, 10);
		err = check_strtol_ret(commit->committer_tz);
		if (err)
			return -1;

		commit->committer_date = 1;
	}

	if (com_time)
		*com_time = commit->committer_time;

	if (com_tz)
		*com_tz = commit->committer_tz;

	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

/**
 * Get the commit's author email.
 * 
 * \param commit git_commit structure to get the author's email
 *               from
 * \param ret pointer to return the author's email into
 * \param len author's email length
 * 
 * \return 0 on success, -1 otherwise
 */
int git_commit_author_email(struct git_commit *commit,
			    const char **ret, size_t *len)
{
	char *p, *endp;

	if (!commit)
		goto out_err;

	p = commit_strstr(commit, "author ");
	if (!p)
		goto out_err;

	p = email_line_start(p);
	if (!p)
		goto out_err;

	endp = email_line_end(p);
	if (!endp)
		goto out_err;

	*ret = ++p;
	if (len)
		*len = endp - p;

	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

/**
 * Get the commit's (author) date
 * 
 * \param commit git_commit structure to get the (author's) date from
 * \param com_time return the date timestamp as seconds since the
 *        epoch, UTC (might be NULL)
 * \param com_tz return the timezone information as the number of
 *               hours and minutes offset from UTC (might be NULL)
 * 
 * \return 0 on succes, -1 otherwise
 */
int git_commit_author_date(struct git_commit *commit,
			   time_t *com_time, int *com_tz)
{
	if (!commit)
		goto out_err;

	if (!com_time && !com_tz)
		goto out_err;

	if (!commit->author_date) {
		int err;
		char *p;

		p = commit_strstr(commit, "author ");
		if (!p)
			goto out_err;

		p = email_line_end(p);
		if (!p)
			goto out_err;

		++p;
		if (*p != ' ')
			goto out_err;

		errno = 0;
		commit->author_time = (time_t) strtoul(++p, NULL, 10);
		err = check_strtoul_ret(commit->author_time);
		if (err)
			return -1;

		p = tz_start(p);
		if (!p)
			goto out_err;

		errno = 0;
		commit->author_tz = strtol(++p, NULL, 10);
		err = check_strtol_ret(commit->author_tz);
		if (err)
			return -1;

		commit->author_date = 1;
	}

	if (com_time)
		*com_time = commit->author_time;

	if (com_tz)
		*com_tz = commit->author_tz;

	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

/**
 * Get the commit's author name.
 * 
 * \param commit git_commit structure to get the author's name
 *               from
 * \param ret pointer to return the author's name into
 * \param len author's name length
 * 
 * \return 0 on success, -1 otherwise
 */
int git_commit_author_name(struct git_commit *commit,
			   const char **ret, size_t *len)
{
	char *p, *endp;

	if (!commit || !ret)
		goto out_err;

	p = commit_strstr(commit, "author ");
	if (!p)
		goto out_err;

	endp = email_line_start(p);
	if (!endp)
		goto out_err;

	--endp;
	if (*endp != ' ')
		goto out_err;

	p += 7;

	*ret = p;
	if (len)
		*len = endp - p;

	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

/**
 * Get the commit's SHA1.
 * 
 * \param commit git_commit structure to get the commit's SHA1 from
 * \param sha1 pointer to an 20-byte array to return the SHA1 into
 * 
 * \return 0 on success, -1 otherwise
 */
int git_commit_id(struct git_commit *commit, unsigned char *sha1)
{
	if (!commit || !commit->commit || !sha1) {
		errno = EINVAL;
		return -1;
	}

	memcpy(sha1, commit->commit->object.sha1, 20);
	return 0;
}

/**
 * Get the commit's tree SHA1.
 * 
 * \param commit git_commit structure to get the tree SHA1 from
 * \param sha1 pointer to an 20-byte array to return the SHA1 into
 * 
 * \return 0 on success, -1 otherwise
 */
int git_commit_tree(struct git_commit *commit, unsigned char *sha1)
{
	if (!commit || !sha1)
		goto out_err;

	if (!commit->tree_id) {
		char *p;
		int err;

		p = commit_strstr(commit, "tree ");
		if (!p)
			goto out_err;

		p += 5;
		if (!p)
			goto out_err;

		err = get_sha1_hex(p, commit->tree_sha1);
		if (err)
			return -1;

		commit->tree_id = 1;
	}

	memcpy(sha1, commit->tree_sha1, 20);
	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

/**
 * Get the commit's buffer raw data.
 * 
 * \param commit git_commit structure to get the raw buffer
 *               from
 * 
 * \return A pointer to the commit's raw buffer (if any).
 */
const char *git_commit_raw(struct git_commit *commit)
{
	if (!commit || !commit->commit) {
		errno = EINVAL;
		return NULL;
	}

	return commit->commit->buffer;
}

/**
 * Free all the memory allocated by the git_commit structure's
 * members.
 * 
 * This is a low-level function, only use it if you know
 * what you're doing.
 * 
 * \param commit git_commit structure to have the contents
 * freed
 */
void __git_commit_free(struct git_commit *commit)
{
	if (!commit->commit)
		return;

	free(commit->commit->buffer);
	commit->commit->buffer = NULL;
}

/**
 * Free all the memory allocated by a git_commit structure.
 * 
 * \param commit git_commit structure to be freed.
 */
void git_commit_free(struct git_commit *commit)
{
	if (commit) {
		__git_commit_free(commit);
		free(commit);
	}
}

/**
 * Initialize a git_commit structure.
 * 
 * This is a low-level function, only use it if you know
 * what you're doing.
 * 
 * \param commit git_commit structure to be initialized
 */
void __git_commit_init(struct git_commit *commit)
{
	commit->commit = NULL;
	commit->author_date = 0;
	commit->committer_date = 0;
	commit->tree_id = 0;
}

/**
 * Allocate and initialize a git_commit structure.
 * 
 * Should be called before using git_revlist_next().
 * 
 * \return A pointer to an allocated git_commit structure
 * on success, NULL otherwise.
 */
struct git_commit *git_commit_init(void)
{
	struct git_commit *commit;

	commit = malloc(sizeof(*commit));
	if (!commit)
		return NULL;

	__git_commit_init(commit);
	return commit;
}

/**
 * Compare two commits.
 * 
 * \param a git_commit structure to compare with
 * \param b git_commit structure to compare with
 * 
 * \return 1 if commits are equal, 0 if they don't and -1 on
 * error.
 */
int git_commit_equal(const struct git_commit *a,const struct git_commit *b)
{
	if (!a || !b) {
		errno = EINVAL;
		return -1;
	}

	return (a->commit == b->commit);
}

/**
 * Get the raw GIT commit object from a git_commit
 * structure.
 * 
 * This is a low-level function, only use it if you know
 * what you're doing.
 * 
 * \param commit git_commit structure to get the commit object
 *               from
 * 
 * \return A pointer to the raw GIT commit object if any,
 * NULL otherwise.
 */
struct commit *__git_commit_obj(struct git_commit *commit)
{
	return commit->commit;
}

/**
 * Set the git_commit structure's (raw) GIT commit object
 * contents.
 * 
 * This is a low-level function, only use it if you know
 * what you're doing.
 * 
 * \param commit git_commit structure to set the raw GIT
 *               commit object
 * \param new_com The raw GIT commit object to set into the git_commit
 *                structure
 */
void __git_commit_obj_set(struct git_commit *commit, struct commit *new_com)
{
	commit->commit = new_com;
}

/**
 * Return a filled git_commit structure from a given commit's SHA1
 * 
 * \param sha1 commit's SHA1 to be read
 * 
 * \returns git_commit structure on success, NULL otherwise.
 */
struct git_commit *git_commit_lookup(const unsigned char *sha1)
{
	struct git_commit *commit;

	if (!sha1) {
		errno = EINVAL;
		return NULL;
	}

	commit = git_commit_init();
	if (!commit)
		return NULL;

	commit->commit = lookup_commit_reference_gently(sha1, 1);
	if (!commit->commit) {
		git_commit_free(commit);
		return NULL;
	}

	return commit;
}

/**
 * Convernt a git_commit string into a real one
 * 
 * git_commit functions doesn't return a '\0' terminated string,
 * in actuality they return a pointer to the string inside the
 * commit's low-level buffer.
 * 
 * This function can be used to convert that pointer into a real
 * ('\0' terminated string).
 * 
 * \param src pointer to the string to be converted
 * \param len string's length
 * 
 * \returns a new allocated '\0' terminated string on success,
 * NULL otherwise
 */
char *git_commit_to_str(const char *src, size_t len)
{
	char *p;

	if (!src || !len) {
		errno = EINVAL;
		return NULL;
	}

	p = malloc(len + 1);
	if (!p)
		return NULL;

	memcpy(p, src, len);
	p[len] = '\0';

	return p;
}
