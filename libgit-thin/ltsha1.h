#ifndef LT_SHA1_H
#define LT_SHA1_H

#define GIT_SHA1_SIZE 20
#define GIT_HEX_LENGTH 40

int git_hex_to_sha1(const char *hex, unsigned char *sha1);
int git_sha1_to_hex(const unsigned char *sha1, char *hex);

#endif /* LT_SHA1_H */
