#ifndef LT_REPO_H
#define LT_REPO_H

int git_repo_open(const char *path);
int git_repo_translate_ref(const char *ref, unsigned char *sha1);
int git_repo_head(unsigned char *sha1);
int git_repo_blob_read(unsigned char *sha1, void **buf, size_t *size);
int git_repo_commit_read(unsigned char *sha1, void **buf, size_t *size);
int git_repo_tree_read(unsigned char *sha1, void **buf, size_t *size);
int git_repo_tag_read(unsigned char *sha1, void **buf, size_t *size);

#endif /* LT_REPO_H */
