/*
 * A tiny git-log like example program
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "libgit-thin.h"

int main(int argc, char *argv[])
{
	int ret;
	struct git_commit *commit;
	struct git_revlist_opt *opt;
	unsigned char sha1[GIT_SHA1_SIZE];

	if (argc < 2 || argc > 3) {
		printf("%s: <git-repo> [commit]\n", argv[0]);
		exit(1);
	}

	ret = git_repo_open(argv[1]);
	if (ret) {
		perror("git_repo_open()");
		exit(1);
	}

	if (argc == 3)
		ret = git_hex_to_sha1(argv[2], sha1);
	else
		ret = git_repo_translate_ref("HEAD", sha1);

	if (ret) {
		perror("could not get sha1");
		exit(1);
	}

	opt = git_revlist_init();
	if (!opt) {
		perror("git_revlist_init()");
		exit(1);
	}

	ret = git_revlist_include(opt, sha1);
	if (ret) {
		perror("git_revlist_include()");
		git_revlist_free(opt);
		exit(1);
	}

	commit = git_commit_init();
	if (!commit) {
		perror("git_commit_init()");
		git_revlist_free(opt);
		exit(1);
	}

	for (;;) {
		ret = git_revlist_next(opt, commit);
		if (ret != 1)
			break;

		printf("%s\n", git_commit_raw(commit));
	}

	if (ret < 0)
		perror("git_revlist_next()");

	git_commit_free(commit);
	git_revlist_free(opt);

	return ret;
}
