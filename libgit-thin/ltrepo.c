/**
 * @file
 * 
 * Repository handling functions.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <cache.h>

#include "ltrepo.h"

/**
 * Open a GIT repository.
 * 
 * This function must be called before anything else, note that it
 * does change the process' current working directory.
 * 
 * \param path relative or absolute repository's path
 * 
 * \returns 0 on success, -1 otherwise.
 */
int git_repo_open(const char *path)
{
	int err;

	if (!path) {
		errno = EINVAL;
		return -1;
	}

	err = chdir(path);
	if (err)
		return -1;

	return git_config(git_default_config);
}

/**
 * Translate a repository's 'ref' into a SHA1
 * 
 * \param ref ref to be translated
 * \param sha1 pointer to a 20-byte array to return the SHA1 into
 * 
 * \returns 0 on success, -1 on error.
 */
int git_repo_translate_ref(const char *ref, unsigned char *sha1)
{
	if (!ref || !sha1) {
		errno = EINVAL;
		return -1;
	}

	return get_sha1(ref, sha1);
}

/**
 * Get repository's HEAD SHA1
 * 
 * \param sha1 pointer to a 20-byte array to return the HEAD
 *             SHA1 into
 * 
 * \returns 0 on success, -1 on error.
 */
int git_repo_head(unsigned char *sha1)
{
	return git_repo_translate_ref("HEAD", sha1);
}

static int repo_read_object(unsigned char *sha1, int type,
			    void **buf, size_t *size)
{
	void *p;
	int mtype;
	unsigned long msize;

	if (!sha1 || (!buf && !size))
		goto out_err;

	p = read_sha1_file(sha1, &mtype, &msize);
	if (!p)
		return -1;

	if (mtype != type) {
		free(p);
		goto out_err;
	}

	if (buf)
		*buf = p;
	if (size)
		*size = (size_t) msize;

	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

/**
 * Read blob's contents into memory.
 * 
 * \param sha1 blob's SHA1
 * \param buf pointer to return the blob's contents (should be freed
 *            with free(), might be NULL)
 * \param size pointer to return the blob's size (might be NULL)
 * 
 * \returns 0 on success, -1 otherwise
 */
int git_repo_blob_read(unsigned char *sha1, void **buf, size_t *size)
{
	return repo_read_object(sha1, OBJ_BLOB, buf, size);
}

/**
 * Read commit's contents into memory.
 * 
 * \param sha1 commit's SHA1
 * \param buf pointer to return the commit's contents (should be
 *            freed with free(), might be NULL)
 * \param size pointer to return the commit's size (might be NULL)
 * 
 * \returns 0 on success, -1 otherwise
 */
int git_repo_commit_read(unsigned char *sha1, void **buf, size_t *size)
{
	return repo_read_object(sha1, OBJ_COMMIT, buf, size);
}

/**
 * Read tree's contents into memory.
 * 
 * \param sha1 tree's SHA1
 * \param buf pointer to return the tree's contents (should be
 *            freed with free(), might be NULL)
 * \param size pointer to return the tree's size (might be NULL)
 * 
 * \returns 0 on success, -1 otherwise
 */
int git_repo_tree_read(unsigned char *sha1, void **buf, size_t *size)
{
	return repo_read_object(sha1, OBJ_TREE, buf, size);
}

/**
 * Read tag's contents into memory.
 * 
 * \param sha1 tag's SHA1
 * \param buf pointer to return the tag's contents into (should be
 *            freed with free(), might be NULL)
 * \param size pointer to return the tag's size (might be NULL)
 * 
 * \returns 0 on success, -1 otherwise
 */
int git_repo_tag_read(unsigned char *sha1, void **buf, size_t *size)
{
	return repo_read_object(sha1, OBJ_TAG, buf, size);
}
