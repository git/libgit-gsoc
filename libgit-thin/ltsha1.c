/**
 * @file
 * 
 * SHA-1 handling related functions
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <string.h>
#include <errno.h>

#include <cache.h>

#include "ltsha1.h"

/**
 * Convert a 40-byte hexadecimal string SHA1 representation into
 * a (raw) 20 byte sequence.
 * 
 * \param hex hexdecimal string to be converted
 * \param sha1 20-byte array to return the conversion result into
 *
 * \return 0 on success, -1 otherwise.
 */
int git_hex_to_sha1(const char *hex, unsigned char *sha1)
{
	if (!hex || !sha1) {
		errno = EINVAL;
		return -1;
	}

	return get_sha1_hex(hex, sha1);
}

/**
 * Convert a (raw) 20-byte SHA1 sequence into a 40-byte hexadecimal
 * string representation.
 * 
 * \param sha1 20-byte SHA1 array to be converted
 * \param hex 41-byte array to be filled with the 40-byte hexadecimal
 *            representation plus the trailing \\0 character
 *
 * \return 0 on success, -1 otherwise.
 */
int git_sha1_to_hex(const unsigned char *sha1, char *hex)
{
	char *p;

	if (!sha1 || !hex) {
		errno = EINVAL;
		return -1;
	}

	p = sha1_to_hex(sha1);
	if (!p)
		return -1;

	strncpy(hex, p, GIT_HEX_LENGTH+1);
	return 0;
}
